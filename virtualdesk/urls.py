from django.urls import path, include
from .views import ReportsList

urlpatterns = [
    path('',ReportsList.as_view(), name='home' )
]