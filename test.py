#!/usr/bin/env python

from tkinter import *
from tkinter import ttk
from tools.widgets import radiobutton

if __name__ == "__main__":
    root = Tk()
    frame = Frame(root, width=100, height=500)
    var1 = radiobutton(frame, text='var1', value='var1')
    var2 = radiobutton(frame, text='var2', value='var2')
    frame.pack(expand=NO, fill=BOTH)
    root.mainloop()
