#!/usr/bin/env python
'''
######################################################################
create a label+entry+ checkbox row frame, with optional file open browser button;
######################################################################
'''

from tkinter import *
from tkinter.filedialog import askopenfilename
from tkinter.messagebox import  showerror, showinfo
from makedicts import makedicts
Queries = {
'RANGE' : 'SELECT * FROM {} WHERE {} BETWEEN {} AND {}',
'SPECIFIC-SEARCH' : 'SELECT * FROM {} WHERE {}',
'UPDATE' : 'UPDATE {} SET {} WHERE {}',
'DELETE' : 'DELETE FROM {} WHERE {}',
'INSERT' : 'INSERT INTO {} VALUES ({})',
'SEARCH' : 'SELECT * FROM {} WHERE {}'
}
TABLE = 'product'
DICTT = {}
def makeSearchForm(parent,label,width=20,browse=True, extend=False):
    var = StringVar()
    var1 = IntVar()
    var1.set(0)
    row = Frame(parent)
    chk = Checkbutton(row, text='',variable=var1)
    lbl = Label(row, text=label + "?", relief=RIDGE, width=width)
    ent = Entry(row, relief=SUNKEN, textvariable=var)
    row.pack(fill=X)

    ent.config(bd=5,width=width)
    ent.pack(side=RIGHT, expand=YES, fill=X)

    lbl.config(bd=5,padx=2,pady=2)
    lbl.pack(side=RIGHT)

    chk.config(bd=3,padx=2,pady=2)
    chk.pack(side=RIGHT)





    if browse:
        btn = Button(row, text='browse...')
        btn.pack(side=RIGHT)
        if not extend:
            btn.config(command= lambda : var.set(askopenfilename() or var.get() ))
        else:
            btn.config(command = lambda : var.set( var.get() + ' ' + askopenfilename()))
    return (var1, var)

def query_selector(parent, queries):
    frm = Frame(parent)
    frm.config(bd=3,padx=2,pady=2)
    frm.pack()
    var = StringVar()
    for q in queries.keys():
        Radiobutton(frm,text=q, variable=var, value=q).pack(side=LEFT)
    return var


class queryDlg(Frame):
    def __init__(self,obj,parent=None):
        Frame.__init__(self,parent)
        self.instance_ = obj
        self.config(padx=3,pady=3)
        msg = 'FORMULATE YOUR QUERIES HERE USE THE RADIO BUTTONS AND CHECKBUTTONS TO BUILD QUERY.'
        lbl = Label(self,text=msg)
        lbl.config(bd=3,relief=RIDGE,padx=3,pady=3)
        lbl.pack(side=TOP,expand=NO,fill=BOTH)
        self.query = query_selector(self,Queries)
        self.var1 = makeSearchForm(self,'InvoiceNo',browse=False)
        self.var2 = makeSearchForm(self,'Stock Code',browse=False)
        self.var3 = makeSearchForm(self,'Description',browse=False)
        self.var4 = makeSearchForm(self,'Quantity',browse=False)
        self.var5 = makeSearchForm(self,'Invoice Date',browse=False)
        self.var6 = makeSearchForm(self,'Unit price',browse=False)
        self.var7 = makeSearchForm(self,'CustomerID',browse=False)
        self.var8 = makeSearchForm(self,'Country',browse=False)
        frm = Frame(self)
        frm.pack(side=RIGHT)
        Button(frm,text='Cancel',command=self.reset).pack(side=RIGHT)
        Button(frm,text='Ok', command=self.validate).pack(side=RIGHT)

        self.payload = (self.query,[self.var1,self.var2,self.var3,self.var4,self.var5,self.var6,self.var7,self.var8])
    
    def validate(self):
        temp = {}
        for var in self.payload[1]:
            flag, value = var[0].get(), var[1].get()
            if var  == self.var1:
                if flag:
                    val = value
                    try:
                        if val:
                            temp['InvoiceNo'] = val
                    except Exception as err:
                        showerror('Type Error', err)
                        print(err)
            if var == self.var2:
                if flag:
                    val = value
                    try:
                        if val:
                            temp['StockCode'] = val
                    except Exception as err:
                        showerror('Type Error', err)
            if var == self.var3:       
                if flag:
                    val = value
                    try:
                        if val:
                            temp['Description'] = val
                    except Exception as err:
                        showerror('Type Error', err)
            if var == self.var4:
                if flag:
                    val = value
                    try:
                        if val:
                            temp['Quantity'] = val
                    except Exception as err:
                        showerror('Type Error', err)

            if var == self.var5:
                if flag:
                    val = value
                    try:
                        if val:
                            temp['InvoiceDate'] = val
                    except Exception as err:
                        showerror('Type Error', err)

            if var == self.var6:
                if flag:
                    val = value
                    try:
                        if val:
                            temp['UnitPrice'] = val
                    except Exception as err:
                        showerror('Type Error', err)
            
            if var == self.var7:
                if flag:
                    val = value
                    try:
                        if val:
                            temp['CustomerId'] = val
                    except Exception as err:
                        showerror('Type Error', err)

            if var == self.var8:
                if flag:
                    val = value
                    try:
                        if val:
                            temp['Country'] = val
                    except Exception as err:
                        showerror('Type Error', err)
        if not self.query.get():
            showerror('Logic Error','Please select operation type')
            return
        global DICTT
        DICTT = temp
        self.clean()
        self.payload[0].set(0)
        for var1, var in self.payload[1]:
            var1.set(0)
            var.set('')
    def reset(self):
        self.payload[0].set(0)
        for var1, var in self.payload[1]:
            var1.set(0)
            var.set('')

    def clean(self):
        global DICTT
        data = DICTT
        QUERY = self.query.get()
        names = [name for name in data.keys()]
        values = [val for val in data.values()]
        row = dict(zip(names,values))

        if QUERY not in Queries.keys():
            showinfo('Query Error','Unknown command\nEmpty checked input field.')
        elif QUERY == 'INSERT':
            placeholder = []
            for v in values:
                placeholder.append('%s')
            query = Queries[QUERY]
            query = query.format(TABLE,','.join(placeholder))
            self.update_table(query, params=values)
        elif QUERY == 'SEARCH':
            command = []
            for i in range(len(values)):
                command.append(f'{names[i]} = \'{values[i]}\'')

            if len(command) == 1:
                a = command[0]
                condition = f'{a}'
                query = Queries[QUERY]
                query = query.format(TABLE,condition)
                result = makedicts(self.instance_.cur,query)
                
                pack = ''
                for data in result:
                    pack += '{}\n'.format("\t\t".join([str(var) for var in data.values()]))   
                self.instance_.queries_feeback.write(pack)

            if len(command) == 2:
                a = command[0]
                b = command[1]
                condition = f'{a} AND {b}'
                query = Queries[QUERY]
                query = query.format(TABLE,condition)
                result = makedicts(self.instance_.cur,query)
                pack = ''
                for data in result:
                    pack += '{}\n'.format("\t\t".join([str(var) for var in data.values()]))   
                self.instance_.queries_feeback.write(pack)
        elif QUERY == 'DELETE':
            command = []
            for i in range(len(values)):
                command.append(f'{names[i]} = \'{values[i]}\'')
            if len(command) == 1:
                a = command[0]
                condition = f'{a}'
                query = Queries[QUERY]
                query = query.format(TABLE,condition)
                query_search = Queries['SEARCH']
                query_search = query_search.format(TABLE,condition)
                result = makedicts(self.instance_.cur,query_search)
                pack = ''
                for data in result:
                    pack += '{}\n'.format("\t\t".join([str(var) for var in data.values()])) 
                self.update_table(query, params=values)
                self.instance_.queries_feeback.write(pack)

            if len(command) == 2:
                a = command[0]
                b = command[1]
                condition = f'{a} AND {b}'
                query = Queries[QUERY]
                query = query.format(TABLE,condition)

                query_search = Queries['SEARCH']
                query_search = query_search.format(TABLE,condition)
                result = makedicts(self.instance_.cur,query_search)
                pack = ''
                for data in result:
                    pack += '{}\n'.format("\t\t".join([str(var) for var in data.values()])) 
                self.update_table(query, params=values)
                self.instance_.queries_feeback.write(pack)

        elif QUERY == 'UPDATE':
            print('Here')
            command = []
            where_clause = []
            for i, name in enumerate(names):
                if name == 'InvoiceNo' or name == 'CustomerId':
                    where_clause.append(f'{names[i]} = \'{values[i]}\'')
                else:
                    command.append(f'{names[i]} = \'{values[i]}\'')
            if len(where_clause) < 2:
                showmessage('Hint','Missing fields please make sure you set the invoice and customer id for the where clause.')
            if len(command) == 1:
                a = command[0]
                condition = f'{a}'
                query = Queries[QUERY]
                query = query.format(TABLE,' , '.join(where_clause),condition)
                self.update_table(query)

                query_search = Queries['SEARCH']
                query_search = query_search.format(TABLE,' AND '.join(where_clause))
                result = makedicts(self.instance_.cur,query_search)
                pack = ''
                for data in result:
                    pack += '{}\n'.format("\t\t".join([str(var) for var in data.values()]))   
                self.instance_.queries_feeback.write(pack)

            if len(command) == 2:
                a = command[0]
                b = command[1]
                condition = f'{a} AND {b}'
                query = Queries[QUERY]
                query = query.format(TABLE,' , '.join(where_clause),condition)
                self.update_table(query)
                
                query_search = Queries['SEARCH']
                query_search = query_search.format(TABLE,' AND '.join(where_clause))
                result = makedicts(self.instance_.cur,query_search)
                pack = ''
                for data in result:
                    pack += '{}\n'.format("\t\t".join([str(var) for var in data.values()]))   
                self.instance_.queries_feeback.write(pack)
                
                

    def update_table(self,query='', params=()):
        if not query:
            showinfo('Query Error', 'Please enter a sound query')
        else:
            try:
                self.instance_.cur.execute(query,params)
                self.instance_.conn.commit()
                showinfo('Sucess', 'Sucessful update and commit')
            except Exception as err:
                self.instance_.conn.rollback()
                showerror('Error',err)
            return self.instance_.cur.rowcount

if __name__ == '__main__':
    root = Tk()
    queryDlg(root).pack()
    mainloop()