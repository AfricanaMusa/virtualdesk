#!/usr/bin/env python

#exctrac pdf information from file
from PyPDF2 import PdfFileReader
from pdfminer.pdfparser import PDFParser
from pdfminer.pdfdocument import PDFDocument
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from io import StringIO
from pdfminer.pdfpage import PDFPage, PDFTextExtractionNotAllowed
from pdfminer.pdfinterp import PDFResourceManager
from pdfminer.pdfinterp import PDFPageInterpreter
from pdfminer.pdfdevice import PDFDevice

def extract_text(pdf_path):
    # open a PDF file
    fp = open(pdf_path, 'rb')
    # Create a PDF parser object associated with the file object
    parser = PDFParser(fp)
    # create a pdf document object that stores the document structure.
    # Supplu the password for initialization
    try:
        document = PDFDocument(parser)
    except Exception:
        pass
    if not document.is_extractable:
        raise PDFTextExtractionNotAllowed
    # layout
    layout = LAParams()
    # bytesio
    retstr = StringIO
    # create resource manager to store shared resources
    rsrcmgr = PDFResourceManager()
    # create a PDF device object
    codec = 'utf-8'
    device = TextConverter(rsrcmgr, retstr, laparams=layout)
    # create inpterpreter for pdf
    interpreter = PDFPageInterpreter(rsrcmgr, device)
    for page in PDFPage.create_pages(document):
        interpreter.process_page(page)
    txt = retstr.getvalue()
    retstr.close()
    rsrcmgr.close()
    device.close()
    fp.close()
    return txt

def extract_info(pdf_path):
    with open(pdf_path, 'rb') as f:
        pdf = PdfFileReader(f)
        information = pdf.getDocumentInfo()
        number_of_pages = pdf.getNumPages()

    txt = f'''

    Author: {information.author}
    Creator: {information.creator}
    Producer: {information.producer}
    Subject: {information.subject}
    Title: {information.title}
    number of pages: {number_of_pages}
    '''
    #print(txt)
    return information

def extract_txt(pdf_path):
    txt = ''
    with open(pdf_path, 'rb') as f:
        pdf = PdfFileReader(f)
        number_of_pages = pdf.getNumPages()
        for page in range(number_of_pages):
            pageObj = pdf.getPage(page)
            txt += pageObj.extractText()
    return txt
if __name__ == '__main__':
    path = r'Mark Lutz - Programming Python-O’Reilly Media (2011).pdf'
    #extract_info(path)
    text = extract_text(path)
    print(text[:5000])
    