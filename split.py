#!/usr/bin/env python
'''
####################################################################
split a file into a set of parts; join.py puts them back together;
this is a customizable version of the standard version of the
standard Unix split command-line split command-line  utility; 
because it is written in python, it also works on Windows and 
can be easly modified because it exports a fuction, it's logic can
be also imported and reused in other applications;
####################################################################
'''
