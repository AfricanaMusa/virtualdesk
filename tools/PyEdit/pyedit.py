#!/usr/bin/python
'''
convenience script to launch pyedit fro arbitrary with the import path set
as required; sys.path for imports and open() mustbe relative to the know top-level
script's dir, not cwd -- cwd is cript's dir if run by shortcut or icon window: 
use argv path; this is a pyw to suppress console pop-up windows; ass this script's
dir to your system PATH to run from command lines; works on Unix too: / and \ handled
partably;
'''

import sys, os
mydir = os.path.dirname(sys.argv[0])
sys.path.insert(1, os.sep.join([mydir] + ['...']*3 ))
exec(open(os.path.join(mydir, 'textEditor.py')).read())