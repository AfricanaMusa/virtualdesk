#!/usr/bin/env python
'''
######################################################################
create a label+entry row frame, with optional file open browe button;
######################################################################
'''

from tkinter import *
from tkinter import ttk
from tkinter.filedialog import askopenfilename
from tools.datepicker import Datepicker
class Placeholder_State(object):
     __slots__ = 'normal_color', 'normal_font', 'placeholder_text', 'placeholder_color', 'placeholder_font', 'with_placeholder'

def add_placeholder_to(entry, placeholder, color="grey", font=None):
    normal_color = entry.cget("fg")
    normal_font = entry.cget("font")
    
    if font is None:
        font = normal_font

    state = Placeholder_State()
    state.normal_color=normal_color
    state.normal_font=normal_font
    state.placeholder_color=color
    state.placeholder_font=font
    state.placeholder_text = placeholder
    state.with_placeholder=True

    def on_focusin(event, entry=entry, state=state):
        if state.with_placeholder:
            entry.delete(0, "end")
            entry.config(fg = state.normal_color, font=state.normal_font)
        
            state.with_placeholder = False

    def on_focusout(event, entry=entry, state=state):
        if entry.get() == '':
            entry.insert(0, state.placeholder_text)
            entry.config(fg = state.placeholder_color, font=state.placeholder_font)
            
            state.with_placeholder = True

    entry.insert(0, placeholder)
    entry.config(fg = color, font=font)

    entry.bind('<FocusIn>', on_focusin, add="+")
    entry.bind('<FocusOut>', on_focusout, add="+")
    
    entry.placeholder_state = state

    return state

def makeFormRow(parent,label,width=25, entry_width=30, browse=True, extend=False, **kwargs):
    var = StringVar()
    row = ttk.Frame(parent)
    lbl = Label(row, text=label, relief=RAISED, width=width, **kwargs)
    lbl.config(height=2, background='grey')
    ent = Entry(row,  textvariable=var, width=entry_width)
    add_placeholder_to(ent,label)
    row.pack(fill=X)
    lbl.pack(side=LEFT, expand=YES,fill=BOTH)
    ent.pack(side=LEFT, expand=YES, fill=BOTH)
    if browse:
        btn = ttk.Button(row, text='browse...', style='TButton')
        btn.pack(side=RIGHT)
        if not extend:
            btn.config(command= lambda : var.set(askopenfilename() or var.get() ))
        else:
            btn.config(command = lambda : var.set( var.get() + ' ' + askopenfilename()))
    return var

def makeFormRowDate(parent,label,width=25, entry_width=30, browse=False, extend=False, **kwargs):
    row = ttk.Frame(parent)
    lbl = Label(row, text=label, relief=FLAT, width=width, **kwargs)
    lbl.config(height=2, background='white')
    ent = Datepicker(row, entrywidth=entry_width, entrystyle="TEntry")
    #add_placeholder_to(ent,label)
    row.pack(fill=X, pady=2)
    lbl.pack(side=LEFT,fill=BOTH)
    ent.pack(side=LEFT, expand=YES, fill=BOTH)
    if browse:
        btn = ttk.Button(row, text='browse...', style='TButton')
        btn.pack(side=RIGHT)
        if not extend:
            btn.config(command= lambda : var.set(askopenfilename() or var.get() ))
        else:
            btn.config(command = lambda : var.set( var.get() + ' ' + askopenfilename()))
    return ent

def makeFormSearch(parent,label,width=15, entry_width=30, browse=True, extend=False, **kwargs):
    var = StringVar()
    row = Frame(parent, background="white")
    lbl = Button(row, text=label, relief=RAISED, width=width, **kwargs)
    lbl.config(height=1, background="grey")
    ent = Entry(row,  textvariable=var, width=entry_width)
    add_placeholder_to(ent,label)
    row.pack(fill=X, pady=3, anchor=NE)
    lbl.pack(side=RIGHT,fill=BOTH, padx=3) 
    ent.pack(side=RIGHT,fill=BOTH)

    #############################
    # bind event
    #############################
    if kwargs:
        ent.bind('<Return>', kwargs['command'])
        lbl.bind('<Return>', kwargs['command'])
    
    if browse:
        btn = ttk.Button(row, text='browse...', style='TButton')
        btn.pack(side=RIGHT)
        if not extend:
            btn.config(command= lambda : var.set(askopenfilename() or var.get() ))
        else:
            btn.config(command = lambda : var.set( var.get() + ' ' + askopenfilename()))
    return var

def makeFormRowString(parent,label,width=25, entry_width=30, browse=True, extend=False, focus=False, passwd=False):
    var = StringVar()
    row = ttk.Frame(parent)
    lbl = Label(row, text=label, relief=FLAT, width=width)
    lbl.config(height=2, background='white')
    ent = Entry(row,  textvariable=var, width=entry_width)
    if focus:
        ent.focus_set()
    if passwd:
        ent.config(show="*")
    add_placeholder_to(ent, label)
    row.pack(fill=X)
    lbl.pack(side=LEFT)
    ent.pack(side=LEFT, expand=YES, fill=BOTH)
    if browse:
        btn = ttk.Button(row, text='browse...', style='S.TButton')
        btn.pack(side=RIGHT)
        if not extend:
            btn.config(command= lambda : var.set(askopenfilename() or var.get() ))
        else:
            btn.config(command = lambda : var.set( var.get() + ' ' + askopenfilename()))
    return var

def makeFormRowInt(parent,label,width=25,entry_width=30, browse=True, focus=False, extend=False):
    var = IntVar()
    row = ttk.Frame(parent)
    lbl = Label(row, text=label, relief=FLAT, width=width)
    lbl.config(height=2, background='white')
    ent = Entry(row,  textvariable=var, width=entry_width)
    if focus:
        ent.focus_set()
    add_placeholder_to(ent, label)
    row.pack(fill=X)
    lbl.pack(side=LEFT)
    ent.pack(side=LEFT, expand=YES, fill=BOTH)
    if browse:
        btn = ttk.Button(row, text='browse...')
        btn.pack(side=RIGHT)
        if not extend:
            btn.config(command= lambda : var.set(askopenfilename() or var.get() ))
        else:
            btn.config(command = lambda : var.set( var.get() + ' ' + askopenfilename()))
    return var

def makeFormRowDouble(parent,label,width=25,browse=True, extend=False):
    var = DoubleVar()
    row = Frame(parent)
    lbl = Label(row, text=label, relief=FLAT, width=width)
    lbl.config(height=3)
    ent = Entry(row, relief=SUNKEN, textvariable=var)
    add_placeholder_to(ent, label)
    row.pack(fill=X)
    lbl.pack(side=LEFT)
    ent.pack(side=LEFT, expand=YES, fill=X)
    if browse:
        btn = Button(row, text='browse...')
        btn.pack(side=RIGHT)
        if not extend:
            btn.config(command= lambda : var.set(askopenfilename() or var.get() ))
        else:
            btn.config(command = lambda : var.set( var.get() + ' ' + askopenfilename()))
    return var

def makeFormRowCombobox(parent,label,options=['216','219','310','409'], width=25):
    var = StringVar()
    row = ttk.Frame(parent)
    lbl = Label(row,text=label,width=width)
    lbl.config()
    lbl.config(padx=1, pady=1,height=2)
    lbl.pack(side=LEFT, expand=YES,fill=BOTH)
    # combobox
    ttk.Combobox(row,textvariable=var, values=options, style='TCombobox').pack(side=LEFT, expand=YES, fill=BOTH)
    row.pack(fill=X)
    return var