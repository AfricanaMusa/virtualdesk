#!/usr/bin/env python
# non-GUI side: connect stream to socket and proceed normally

import time, sys
if len(sys.argv) > 1:   #link to GUI if only requested
    from socket_stream_redirect0 import * # connect my sys.std.out to socket
    redirectOut()               # GUI should be started first as is

# non-GUI code
while True:
    print(time.asctime())       #print data to stdout:
    sys.stdout.flush()          # sent to GUI process via socket
    time.sleep(2.0) 
