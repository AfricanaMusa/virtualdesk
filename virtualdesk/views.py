from django.shortcuts import render
from django.views.generic import ListView
from .models import Reports

# Create your views here.

class ReportsList(ListView):

    context_object_name = 'reports'
    queryset = Reports.objects.all()
    template_name = 'home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['labels'] = ''
        return context

