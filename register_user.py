#!/usr/bin/env python

from tkinter import *
from tkinter import ttk
from tools.widgets import cframe, ccframe
from tools.process_img import pImage
from tools.formrows import makeFormRowString

class registerUser(Toplevel):
    '''
    Top level window that new registers members in to system
    '''
    def __init__(self,parent=None, request=None, image_dir=None, subdir=None):
        Toplevel.__init__(self,parent)
        self.parent = parent
        self.request = request
        self.image_dir = image_dir
        self.subdir = subdir
        self.transient(parent)
        self.makewidgets()
        self.grab_set()
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        self.initial_focus = self
        if not (self.initial_focus):
            self.initial_focus = self
        self.initial_focus.focus_set()
        self.wait_window(self)
    
    def makewidgets(self):
        ############
        # containers
        ############

        container = ccframe(self, "LOG IN", "G.TFrame", "G.TButton")
        photo_container = ttk.Frame(container.body, width=100)
        lbl = pImage(photo_container, self.image_dir,'photo-signup.jpeg', self.subdir)
        lbl.pack(fill=BOTH)
        user_container = Frame(container.body)
        user_container.configure(background='white')
        profile_container = Frame(container.body)
        profile_container.configure(background='white')
        btns_container = Frame(container.body)
        btns_container.configure(background='white')

        ####################
        # forms
        ####################
        user_name = makeFormRowString(user_container, 'user name'.upper(), browse=False)
        first_name = makeFormRowString(user_container, 'first name'.upper(), browse=False)
        last_name = makeFormRowString(user_container, 'last name'.upper(), browse=False)
        email = makeFormRowString(user_container, 'email'.upper(), browse=False)
        photo = makeFormRowString(profile_container, 'user profile'.upper())
        date_of_birth = makeFormRowString(profile_container, 'date of birth'.upper(), browse=False)
        department= makeFormRowString(profile_container, 'department'.upper(), browse=False)
        first_name = makeFormRowString(profile_container, 'first name'.upper(), browse=False)
        date_joined = makeFormRowString(profile_container, 'date joined'.upper(), browse=False)
        gender = makeFormRowString(profile_container, 'gender'.upper(), browse=False)
        id_number = makeFormRowString(profile_container, 'id number'.upper(), browse=False)

        ####################
        # bind event
        ####################
        self.bind('<Return>', self.register_user)
        self.bind('<Escape>', lambda event:self.destroy())

        ###################
        # action buttons
        ###################

        update = ttk.Button(btns_container, text='upload'.upper(), style="L.TButton")
        cancel = ttk.Button(btns_container, text='clear'.upper(), style="S.TButton")
        close = ttk.Button(btns_container, text='exit'.upper(), command=self.destroy, style="S.TButton")

        #####################
        # config and packing
        #####################
        photo_container.pack(expand=YES, fill=BOTH,side=LEFT)
        user_container.pack(expand=YES, fill=BOTH, padx=5)
        profile_container.pack(expand=YES, fill=BOTH, padx=5)

        close.pack(side=RIGHT, fill=X, padx=4,pady=4)
        cancel.pack(side=RIGHT, fill=X, padx=4,pady=4)
        update.pack(side=RIGHT, fill=X, padx=4,pady=4)

        btns_container.pack(expand=YES, fill=BOTH, padx=5)
        
        container.pack(fill=BOTH, expand=YES, padx=5, pady=5)
    
    def register_user(self, event):
        '''
        saves data to db
        '''
        pass