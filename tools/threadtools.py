#!/usr/bin/env python
'''
#############################################################################
System-wide thread interface utilities for GUIs.

Implements a single thread callback queue and checker and checker
timer loop shared by all the windows in a program; worker threads queue
their exit and progress action to be run in the main thread; this doesn't 
block the GUI - it just spawns operations and manages and dispatches xists 
and prgress; worker threads can overlap freely with the main thread, and with 
other workers.

using a queue of callback fucntions and arguments is more useful than a 
simple data queue if thehre can be many kinds of threads running at the same
time - each kindmay have diffrent implied actions.

Because GUI API is notcompletely thread-safe, instead of calling GUI
updated callbaclks directly after thread main action, place the on a shared
queue, to be run from a timer in the main thread, not a child threa;
this also makes GUI update points less random and unpredicatable; requires
threads to be split into main action, exit actions, and progress actions.

Assumes threaded action raises an exception on failure, and has a 'progress'
callback argument if it supports progress updates; also assumers callbacks 
are either short-lived or update as they run, and that queue will contain
callback functions (or other callables) for use in a GUI app -requires a 
widget in order to schedule and catch 'after' event loop callbacks; to use
this model in nono-GUI contexts, could use simple timer instead
################################################################################
'''

#run even if not threads
try:
    import _thread as thread
except ImportError:
    import _dummy_thread as thread

#shared cross-process queue
#named in shared global scoper, lives in shared object memory
import sys, queue
threadQueue = queue.Queue(maxsize=0)


#########################################################################################
# IN MAIN THREAD - priodically check thread completions queue; and run implied GUI
# actions on queue in this main GUI thread; one consumer (GUI), and multiple
#producers (load, del, send); a simple list may siffice too: list.appendand pop atomic
#########################################################################################

def threadChecker(widget, delayMsecs=100, perEvent=1):
    for i in range(perEvent):
        try:
            callback, args = threadQueue.get(block=False)
        except queue.Empty:
            break
        else:
            callback(*args)
    widget.after(delayMsecs,lambda : threadChecker(widget, delayMsecs, perEvent))

#######################################################################################
# IN A NEW THREAD - run action, manage, thread queue puts for exits and progress;
# run action with args now, later run no* calls with context; calls added to 
# queue here are dispatched directly: may update GUI in threadm since passed func in 
# shared memory  called in thread; progrss callback jsut adds call back to queue
# with passed args; don't update in-progress counters here: not finished ti;;
# exit action taken off queue and dispatched in main thread by threadChecker;
########################################################################################

def threaded(action, args, context, onExit, onFail, onProgress):
    try:
        if not onProgress:
            action(*args)
        else:
            def progress(*any):
                threadQueue.put((onProgress, any + context))
            action(progress=progress, *args)
    except:
        threadQueue.put((onFail, (sys.exc_info(),) + context))
    else:
        threadQueue.put((onExit, context))

def startThread(action, args, context, onExit, onFail, onProgress=None):
    thread.start_new_thread(threaded, (action, args, context, onExit, onFail, onProgress))

##########################################################################################
# a thread counter or flag: useful to avoid operation overlap if threads
# update other shared state beyond that managed by the thread call back queue
##########################################################################################

class ThreadCounter:
    def __init__(self):
        self.count = 0
        self.mutex = thread.allocate_lock()
    
    def incr(self):
        self.mutex.acquire()
        self.count += 1
        self.mutex.release()
    
    def decr(self):
        self.mutex.acquire()
        self.count -= 1 
        self.mutex.release()
    
    def __len__(self): return self.count

###################################################################
# self.test code: split thread action into main, exits, progress
###################################################################

if __name__ == "__main__":
    import time
    from tkinter.scrolledtext import ScrolledText

    def onEvent(i):
        myname = 'thread-%s' % i
        startThread(
            action = threadaction,
            args = (i,3),
            context = (myname,),
            onExit = threadexit,
            onFail = threadfail,
            onProgress = threadprogress
        )
    # thread's main action
    def threadaction(id, reps, progress):
        for i in range(reps):
            time.sleep(1)
            if progress: progress(i)
        if id % 2 == 1: raise Exception

    #thread exit/progress callbacks: dispatched off queue in main thread
    def threadexit(myname):
        text.insert('end', '%s\texit\n' % myname)
        text.see('end')

    def threadfail(exc_info, myname):
        text.insert('end', '%s\tfail\t%s\n' % (myname, exc_info))
        text.see('end')
    
    def threadprogress(count, myname):
        text.insert('end', '%s\tprog\t%s\n' % (myname, count))
        text.see('end')
        text.update()

    text = ScrolledText()
    text.pack()
    threadChecker(text)
    text.bind('<Button-1>', lambda event: list(map(onEvent, range(6))))
    text.mainloop()