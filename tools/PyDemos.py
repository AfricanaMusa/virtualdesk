#!/usr/bin/env python

'''
###################################################################################
launch major Python+Tk GUI examples from the book, in a platform-neutral way.
###################################################################################
'''

###################################################################
# start building main GUI windows
###################################################################

from tkinter import *
from tkinter import ttk
from windows import MainWindow
from windows import PopupWindow

Root = MainWindow('Py demos')
#build message window
Stat = PopupWindow('PP4 demo info')
Stat.protocol('WM_DELETE_WINDOW', lambda:0)


Info = Label(Stat, text='Select demo',font=('courier', 20, 'italic'), padx=12, pady=12, bg='lightblue')
Info.pack(expand=YES, fill=BOTH)

################################################
# add launch buttons with callback objects
###############################################

import launchmodes
from simpleedit import SimpleEditor

#demo launch class
class Launcher(launchmodes.PortableLauncher):
    def announce(self,text):
        Info.config(text=text)

def viewer(sources):
    for filename in sources:
        editor = SimpleEditor(Root, filename)

def demoButton(name, what, doit, code):
    '''
    add buttons that runs doit command-line and open all files in code;
    doit button retians state in an object, code in enclosing scope;
    '''
    rowfrm = Frame(Root)
    rowfrm.pack(side=LEFT,expand=YES, fill=BOTH)

    b = Button(rowfrm, bg='navy', fg='white', relief=RIDGE, border=4)
    b.config(text= name, width=20, command=Launcher(what, doit))
    b.pack(side=LEFT, expand=YES, fill=BOTH)

    b = Button(rowfrm, bg='beige', fg='navy', relief=RIDGE)
    b.config(text=name, width=20, command=(lambda : viewer(code)))
    b.pack(side=LEFT)


#####################################################
# tkinter GUI demos - some use network connections
#####################################################

demoButton(
    name='PyEdit',
    what='Text file editor',
    doit='../simpleedit.py',
    code = [
        'launchmodes.py',
    ]
)
demoButton(
    name='PyView',
    what ='Pipe gui',
    doit='pipe_gui3.py',
    code = [
        'pipe_gui3.py',
    ]
)
###############################################
# toggle message box font once a second
###############################################

def refreshMe(info, ncall):
    slant = ['normal', 'italic', 'bold', 'bold italic'][ncall % 4]
    info.config(font=('courier', 20, slant))
    Root.after(1000, (lambda : refreshMe(info, ncall+1)))

########################################################################3
# unhide/hide status box on info clicks
####################################################################

Stat.iconify()
def onInfo():
    if Stat.state() == 'iconic':
        Stat.deiconify()
    else:
        Stat.iconify()

#####################################################################
# finish building main GUI, start event loop
#####################################################################


Button(Root, text='Quit', command=Root.quit).pack(side=BOTTOM, fill=X)
Button(Root, text='Info', command=Info).pack(side=BOTTOM, fill=X)
Button(Root, text='LINKS').pack(side=BOTTOM, fill=X)
refreshMe(Info,0)
Root.mainloop()