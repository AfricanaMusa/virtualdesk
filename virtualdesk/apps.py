from django.apps import AppConfig


class VirtualdeskConfig(AppConfig):
    name = 'virtualdesk'
