#!/usr/bin/env python
'''
profile edit form
'''

from tkinter import *
from tkinter import ttk
from tkinter.messagebox import *
from django.contrib.auth.models import User
from virtualdesk.models import Department
from tools.formrows import *
from tools.process_img import pImage
from tools.widgets import cframe, ccframe

class createDepartment(Toplevel):
    '''
    form for editing department object and user profile 
    '''
    def __init__(self,parent=None, request=None, image_dir=None, subdir=None):
        Toplevel.__init__(self,parent)
        self.parent = parent
        self.request = request
        self.image_dir = image_dir
        self.subdir = subdir
        self.transient(parent)
        self.makewidgets()
        self.grab_set()
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        self.initial_focus = self
        if not (self.initial_focus):
            self.initial_focus = self
        self.initial_focus.focus_set()
        self.wait_window(self)
    
    def makewidgets(self):
        ############
        # containers
        ############

        container = ccframe(self, "LOG IN", "G.TFrame", "G.TButton")
        photo_container = ttk.Frame(container.body, width=100)
        lbl = pImage(photo_container, self.image_dir,'photo-department.jpg', self.subdir)
        lbl.pack(fill=BOTH)
        user_container = Frame(container.body)
        user_container.configure(background='white')
        profile_container = Frame(container.body)
        profile_container.configure(background='white')
        btns_container = Frame(container.body)
        btns_container.configure(background='white')

        ####################
        # forms
        ####################
        self.name = makeFormRowString(user_container, 'name'.upper(), browse=False,focus=True)
        self.description = makeFormRowString(user_container, 'description'.upper(), browse=False)
        self.manager= makeFormRowString(user_container, 'manager'.upper(), browse=False)
        self.members = makeFormRowString(profile_container, 'members'.upper())

        ####################
        # bind event
        ####################
        self.bind('<Return>', self.create_department)
        self.bind('<Escape>', lambda event:self.destroy())

        ###################
        # action buttons
        ###################

        update = ttk.Button(btns_container, text='create'.upper(), style="L.TButton", command=self.create_department)
        cancel = ttk.Button(btns_container, text='clear'.upper(), style="S.TButton")
        close = ttk.Button(btns_container, text='exit'.upper(), command=self.destroy, style="S.TButton")

        #####################
        # config and packing
        #####################
        photo_container.pack(expand=YES, fill=BOTH,side=LEFT)
        user_container.pack(expand=YES, fill=BOTH, padx=5)
        profile_container.pack(expand=YES, fill=BOTH, padx=5)

        close.pack(side=RIGHT, fill=X, padx=4,pady=4)
        cancel.pack(side=RIGHT, fill=X, padx=4,pady=4)
        update.pack(side=RIGHT, fill=X, padx=4,pady=4)

        btns_container.pack(expand=YES, fill=BOTH, padx=5)
        
        container.pack(fill=BOTH, expand=YES, padx=5, pady=5)

    def create_department(self, event=None):
        '''
        allow user to create departments
        '''
        user = User.objects.get(pk=self.request.user.id)
        if user:
            d = Department(
                name = self.name.get() if self.name.get() != 'name'.upper() else None,
                description = self.description.get() if self.description.get() != 'description'.upper() else None,
                manager = user
            )
            d.save()
            d.members.add(user)
            showinfo('Department', 'Department created sucessfully')

        

if __name__ == "__main__":
    editProfile().mainloop()