from django.urls import path, include
from .views import dashboard, register, edit, user_list , user_detail, user_follow

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    path('dashboard',  dashboard.as_view(), name="dashboard"),
    path('accounts/register/', register.as_view(), name="register"),
    path('accounts/edit/',edit.as_view(), name="edit"),
    #path('accounts/', include('social_django.urls', namespace='social')),
    path('accounts/users/',user_list.as_view(), name='user-list' ),
    path('accounts/users/<int:pk>/', user_detail.as_view(), name='user-detail'),
    path('accounts/users/follow', user_follow.as_view(), name='user-follow'),
]