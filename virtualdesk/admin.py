from django.contrib import admin

# Register your models here.

from .models import Reports, Department, Profile, Sector

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('user', 'photo', 'date_of_birth', 'department', 'gender')
    list_filter = ('department', 'gender')
    search_fields = ('user', 'gender', 'department')

class ReportsAdmin(admin.ModelAdmin):
    list_display = ('file_title', 'created_by','created_at','department','file')
    list_filter = ('file_title', 'created_by', 'department')
    search_fields = ('file_title', 'created_by', 'department')

class DepartmentAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'manager')
    list_filter = ('name', 'manager')
    search_fields = ('name', 'manager')

class SectorAdmin(admin.ModelAdmin):
    list_display = ('name', 'description')

admin.site.register(Reports, ReportsAdmin)
admin.site.register(Department, DepartmentAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Sector, SectorAdmin)
