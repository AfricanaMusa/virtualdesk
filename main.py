#!/usr/bin/env python

## main implements a mixin 

# Django specific settings
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "flaconHeavy.settings")

# Ensure settings are read
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from tkinter import *
from tkinter import ttk
from tkinter.messagebox import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.http.request import HttpRequest
from django.conf import settings
from django.utils.module_loading import import_module
from django.contrib.auth.decorators import login_required
from virtualdesk.models import Reports, Sector, Department, Profile
from django.contrib.auth.middleware import AuthenticationMiddleware
from tools.guimaker import GuiMakerWindowMenu
from tools.formrows import makeFormSearch
from login_dialog import loginDialog
from profile_dialog import editProfile
from department_dlg import createDepartment
from sector_dlg import createSector
from report_form_dlg import createReport
from tools.widgets import cframe, borderedFrame, ccframe
from tools.scrollabletable import Table
from virtualdesk.models import Reports, Department, Profile, Sector
from tools.widgets import label, radiobutton
from tools.launchmodes import QuietPortableLauncher



class BaseUI:
    '''
    implements as a class to allow this code to be inherited and extended
    '''
    iconpath = r'Windows Icons'
    image_dir = r'images'
    subdir = r'thumbs'
    def __init__(self, parent=None):
        self.request = HttpRequest()
        session_key = None
        session_engine = import_module(settings.SESSION_ENGINE)
        self.request.session =  session_engine.SessionStore(session_key)
        auth_engine = AuthenticationMiddleware()
        auth_engine.process_request(self.request)
        self.parent = parent
        
    def user_login(self):
        '''
        Authenticate the User and log him or her into the system
        '''
        dlg = loginDialog(self,self.request, self.image_dir, self.subdir)
        self.request = dlg.request
    
    def user_logout(self):
        '''
        logout the user
        '''
        logout(self.request)

    
    def edit_profile(self, request):
        '''
        activate profile dialog
        '''
        if self.request.user.is_authenticated:
            dlg = editProfile(self, request, self.image_dir, self.subdir)
        else:
            showwarning('Profile', 'You need to be logged in the edit your profile')
            self.user_login()
            self.edit_profile(self.request)
    
    def create_department(self):
        '''create department inthe database'''
        if self.request.user.is_authenticated:
            dlg = createDepartment(self, self.request, self.image_dir, self.subdir)
        else:
            showwarning('Profile', 'You need to be logged in to create department')
            self.user_login()
            self.create_department()

    def create_sector(self):
        '''create sector in the database'''
        if self.request.user.is_authenticated:
            dlg = createSector(self, self.request, self.image_dir, self.subdir)
        else:
            showwarning('Sector', 'You need to be logged in to create department')
            self.user_login()
            self.create_sector()

    def create_report(self):
        '''create report inthe database'''
        if self.request.user.is_authenticated:
            dlg = createReport(self, self.request, self.image_dir, self.subdir)
        else:
            showwarning('Profile', 'You need to be logged in to create report')
            self.user_login()
            self.create_report()


    def quit_(self):
        '''
        ask for user confirmation and quit the application
        '''
        if not self.request:
            if askyesno('Exit', 'Do you wish to quit ?'):
                self.quit()
        if askyesno('Close App', 'Do you wish to logout and close the application ?'):
            self.user_logout()
            self.quit()

class Dashboard(Frame):
    '''creates main dashboard for the app'''
    def __init__(self,parent, data=None, request=None, title=None, btn=None, callback=None):
        Frame.__init__(self, parent)
        self.parent = parent
        self.data = data
        self.model = Reports
        self.request = request
        self.callback = callback
        self.filter_var = []
        self.header_lbl = StringVar()
        self.header_btn = StringVar()
        self.header_lbl.set(title.upper())
        self.header_btn.set(btn.upper())
        self.header_btn_ = ''
        self.make_widgets()
        self.pack(fill=BOTH, expand=YES)
    
    def select_data(self):
        data = self.data
        if not data:
            return None, None, None, None
        rows = []
        filters = []
        for index, row in enumerate(data):
            lst = []
            fil = []
            for key in row.keys():
                if key == 'created_by_id':
                    user = User.objects.get(pk=row[key])
                    lst.append(user.username)
                    fil.append((key,user.username))
                elif key == 'department_id':
                    dep = Department.objects.get(pk=row[key])
                    lst.append(dep.name)
                    fil.append((key,dep.name))  
                elif key == 'sector_id':
                    sec = Sector.objects.get(pk=row[key])
                    lst.append(sec.name)
                    fil.append((key,sec.name))
                elif key == 'manager_id':
                    usr = User.objects.get(pk=row[key])
                    lst.append(usr.username)
                    fil.append((key,usr.username))
                elif key == 'user_id':
                    user = User.objects.get(pk=row[key])
                    lst.append(user.username)
                    fil.append((key,user.username))
                else:
                    lst.append(row[key])
            rows.append(lst)
            filters.append(fil)
            try:
                columns = [ key.replace('_', ' ').replace('id', '').title() for key in data[0].keys()]
            except IndexError:
                columns = None
        index = [None] * len(columns)
        return filters, columns, index, rows

    def select_overview(self):
        pass

    def select_department(self):
        '''
        select all departments currently in the database
        '''
        self.model = Department
        self.header_lbl.set('departments'.upper())
        self.header_btn.set('add department'.title())
        self.header_btn_.config(command=self.callback[self.header_btn.get().lower()])
        self.data = Department.objects.values()
        self.table.pack_forget()
        filters, columns , index, rows = self.select_data()
        tbl = Table(self.table_container,columns,#scroll_vertically=False,
                         column_minwidths=index)
        self.table = tbl
        tbl.pack(expand=YES, fill=BOTH)
        if rows:
            tbl.set_data(rows)
        pos = 0 
        pos = 0
        for x, y in zip(self.filter_var, filters):
            for a,b in y:
                x.set(b)
                pos += 1
        for x in range(pos, len(self.filter_var)):
            self.filter_var[x].set("")

    def select_profile(self):
        '''
        select all departments currently in the database
        '''
        self.model = Profile
        self.header_lbl.set('profile'.upper())
        self.header_btn.set('add profile'.title())
        self.header_btn_.config(command= lambda: self.callback[self.header_btn.get().lower()](self.request))
        self.data = Profile.objects.values()
        filters, columns , index, rows = self.select_data()
        tbl = Table(self.table_container,columns,#scroll_vertically=False,
                         column_minwidths=index)
        self.table.pack_forget()
        self.table = tbl
        tbl.pack(expand=YES, fill=BOTH)
        if rows:
            tbl.set_data(rows)
        pos = 0
        for x, y in zip(self.filter_var, filters):
            for a,b in y:
                x.set(b)
                pos += 1

    def select_report(self):
        '''
        select all departments currently in the database
        '''
        self.model = Reports
        self.header_lbl.set('reports'.upper())
        self.header_btn.set('add report'.title())
        self.header_btn_.config(command=self.callback[self.header_btn.get().lower()])
        self.data = Reports.objects.values()
        self.table.pack_forget()
        filters, columns , index, rows = self.select_data()
        tbl = Table(self.table_container,columns,#scroll_vertically=False,
                         column_minwidths=index)
        self.table = tbl
        tbl.pack(expand=YES, fill=BOTH)
        if rows:
            tbl.set_data(rows)
        for x, y in zip(self.filter_var, filters):
            for a,b in y:
                x.set(b)

    def select_sector(self):
        '''
        select all sectors currently in the database
        '''
        self.model = Sector
        self.header_lbl.set('sector'.upper())
        self.header_btn.set('add sector'.title())
        self.header_btn_.config(command=self.callback[self.header_btn.get().lower()])
        self.data = Sector.objects.values()
        self.table.pack_forget()
        filters, columns , index, rows = self.select_data()
        tbl = Table(self.table_container,columns,#scroll_vertically=False,
                         column_minwidths=index)
        self.table = tbl
        tbl.pack(expand=YES, fill=BOTH)
        if rows:
            tbl.set_data(rows)
        for x, y in zip(self.filter_var, filters):
            for a,b in y:
                x.set(b)
    
    def filter_data(self, filter_, ):
        self.search_btn.set(filter_)
        self.search_field.set(search_word)
        self.search()
        
    

    def search(self, event=None):
        if not self.search_btn.get():
            return
        if self.search_word.get():
            data = self.model.objects.values()
            files = []
            for row in data:
                files.append(row['file'])
            for file in files:
                if file.endswith('.pdf'):
                    print(file, '=>', 'pdf')
                elif file.endswith('.txt'):
                    print(file, '=>', 'txt')
                elif file.endswith('.docx'):
                    print(file, '=>', 'docx')
        else:
            if self.search_field.get() == 'Author':
                self.data = self.model.objects.filter(created_by__id=self.search_btn.get()).values()
            if self.search_field.get() == 'title':
                self.data = self.model.objects.filter(file_title__icontains=self.search_btn.get()).values()
            if self.search_field.get() == 'Date':
                self.data = self.model.objects.filter(created_at__icontains=self.search_btn.get()).values()
            if self.search_field.get() == 'Sector':
                self.data = self.model.objects.filter(department__id=self.search_btn.get()).values()

        filters, columns , index, rows = self.select_data()
        if rows:
            self.table.set_data(rows)

    def make_widgets(self):
        container = ccframe(self, ' ', 'G.TButton', 'G.TButton')
        container_ = container.body
        self.container_ = container_

        category_container = Frame(container.body, background='white')
        category_body = ccframe(category_container, 'Nav Bar'.upper(),'G.TFrame', 'G.TButton', True)
        category_body.pack(expand=YES, fill=X)
        category_body1 = Frame(category_body)
        category_body1.pack(side=LEFT, expand=YES, fill=X , padx=2, pady=2)
        overview = borderedFrame(category_body1,command=self.select_overview, bordercolor='green', borderleft=4, interiorwidget=Button, relief=FLAT, text="Overview", background="white")
        overview.pack(side=LEFT, padx=2, pady=2, expand=YES, fill=X)
        Reports = borderedFrame(category_body1,command=self.select_report, bordercolor='green', borderleft=4, interiorwidget=Button, relief=FLAT, text="Reports", background="white")
        Reports.pack(side=LEFT, padx=2, pady=2, expand=YES, fill=X)
        category_body2 = Frame(category_body)
        category_body2.pack(side=LEFT, expand=YES, fill=X , padx=2, pady=2)
        Department = borderedFrame(category_body2,command=self.select_department, bordercolor='green', borderleft=4, interiorwidget=Button, relief=FLAT, text="Department", background="white")
        Department.pack(side=LEFT, padx=2, pady=2, expand=YES, fill=X)
        Sector_ = borderedFrame(category_body2,command=self.select_sector, bordercolor='green', borderleft=4, interiorwidget=Button, relief=FLAT, text="Sector", background="white")
        Sector_.pack(side=LEFT, padx=2, pady=2, expand=YES, fill=X)
        category_body3 = Frame(category_body)
        category_body3.pack(side=LEFT, expand=YES, fill=X , padx=2, pady=2)
        Profiles = borderedFrame(category_body3,command=self.select_profile, bordercolor='green', borderleft=4, interiorwidget=Button, relief=FLAT, text="Profile", background="white")
        Profiles.pack(side=LEFT, padx=2, pady=2, expand=YES, fill=X)
        category_container.grid(row=0, column=0,columnspan=9, pady=2, sticky=EW)
        ############################################################

        header = Frame(container.body, background='white')
        lbl = ttk.Label(header, style="H.TLabel", textvariable=self.header_lbl)
        btn = ttk.Button(header, style="S.TButton",textvariable=self.header_btn, command=self.callback[self.header_btn.get().lower()])
        lbl.pack(side=LEFT)
        btn.pack(side=RIGHT)
        self.header_btn_ = btn
        header.grid(row=1, column=0,columnspan=9, pady=4, sticky=NSEW)



        table_container = Frame(container.body, background='white')
        self.table_container = table_container
        search_container = Frame(table_container, background='white')
        self.search_btn = makeFormSearch(search_container, "SEARCH", browse=False, command=self.search)
        self.search_btn.set('')
        options_frame = Frame(search_container, background="white")
        op_lbl = label(options_frame, LEFT, "search fields")
        self.search_field = StringVar()
        rad0 = radiobutton(options_frame, text=' Report Number',value='On', side=LEFT, background='white', variable=self.search_field)
        rad1 = radiobutton(options_frame, text=' Report Title',value='title', side=LEFT, background='white', variable=self.search_field)
        rad2 = radiobutton(options_frame, text='Created By',value='Author', side=LEFT, background='white', variable=self.search_field)
        rad3 = radiobutton(options_frame, text='Date Created',value='Date', side=LEFT, background='white', variable=self.search_field)
        rad4 = radiobutton(options_frame, text='Sector',value='Sector', side=LEFT, background='white', variable=self.search_field)
        self.search_field.set('title')
        options_subframe = Frame(search_container, background='white')
        opt_sub_lbl = label(options_subframe, LEFT, 'Search specific word')
        self.search_word = BooleanVar()
        self.search_word.set(0)
        sub_rad1 = radiobutton(options_subframe, text='Yes',value=1, side=LEFT, background='white', variable=self.search_word)
        sub_rad2 = radiobutton(options_subframe, text='No',value=0, side=LEFT, background='white', variable=self.search_word)

        ###############################
        # pack widgets
        ###############################

        options_frame.pack(pady=2, anchor=NE)
        options_subframe.pack(pady=2, anchor=NE)
        search_container.pack(pady=3, anchor=NE, fill=X)
        
        filters, columns , index, rows = self.select_data()
        tbl = Table(table_container,columns,#scroll_vertically=False,
                         column_minwidths=index)
        self.table = tbl
        tbl.pack(expand=YES, fill=BOTH)
        if rows:
            tbl.set_data(rows)

        filter_container = Frame(container.body, background='white')
        filter_body = ccframe(filter_container, 'FILTERS', 'B.TButton', 'B.TButton')
        processed_combo = []
        processed_key = []
        processed_filter = []
        iteration = 0
        for filterr in filters:
            for key, fil in filterr:
                var = StringVar()
                if key not in processed_key:
                    filter_categ_body = ccframe(filter_body.body, key.replace('_id', '').upper(),'G.TFrame','G.TButton')
                    borderedFrame(filter_categ_body.body, bordercolor='green', borderleft=3, interiorwidget=Button, relief=FLAT, textvariable=var, background="white")
                elif key not in processed_key and iteration == 0:
                    filter_categ_body = ccframe(filter_body.body, key.replace('_id', '').upper(),'G.TFrame','G.TButton')
                    borderedFrame(filter_categ_body.body, bordercolor='green', borderleft=3, interiorwidget=Button, relief=FLAT, textvariable=var, background="white")
                else:
                    if fil in processed_filter:
                        continue
                    pos = processed_key.index(key)
                    filter_categ_body = processed_combo[pos][1]
                    borderedFrame(filter_categ_body.body, bordercolor='green', borderleft=3, interiorwidget=Button, relief=FLAT, textvariable=var, background="white")
                var.set(fil)  
                processed_key.append(key)
                processed_filter.append(fil)
                processed_combo.append((key,filter_categ_body))  
                self.filter_var.append(var)



        container.grid(row=0,column=0, rowspan=5, columnspan=7, sticky=NSEW)
        table_container.grid(row=3, column=3, columnspan=3, pady=4, sticky=NSEW)
        filter_container.grid(row=3, column=0,  columnspan=3 ,pady=4, sticky=NSEW)
        container_.columnconfigure(0,weight=2)
        container_.columnconfigure(1,weight=2)
        container_.columnconfigure(2,weight=2)
        container_.columnconfigure(3,weight=2)
        container_.columnconfigure(4,weight=2)
        container_.columnconfigure(5,weight=2)
        container_.rowconfigure(1,weight=3)
        self.columnconfigure(0, weight=3)
        self.rowconfigure(0, weight=3)

    def configure_win(self, e=None):
        self.container_.columnconfigure(0,weight=2)
        self.container_.columnconfigure(1,weight=2)
        self.container_.columnconfigure(2,weight=2)
        self.container_.columnconfigure(3,weight=2)
        self.container_.columnconfigure(4,weight=2)
        self.container_.columnconfigure(5,weight=2)
        self.container_.rowconfigure(1,weight=3)
        self.columnconfigure(0, weight=3)
        self.rowconfigure(0, weight=3)


class vdesk(GuiMakerWindowMenu, BaseUI):
    '''
    Main GUI window
    '''
    def __init__(self,parent=None):
        BaseUI.__init__(self,parent)
        GuiMakerWindowMenu.__init__(self,parent)
        self.master.protocol("WM_DELETE_WINDOW", self.quit_)
    
    def start(self):
        '''
        overrides GuiMakerWindowMenu start to create Menu and toolbars
        '''
        menuBar = [('File',0,[('Log In', 0, self.user_login),
                    ('Log Out', 0, self.user_logout),
                    ('Close App', 0, self.quit_)])]
        quickActions = [('managers', None, {'side':LEFT}),
                    ('supervisors', None, {'side':LEFT}),
                    ('Depertments', None, {'side':LEFT}),
                    ('Reports', None, {'side':LEFT})]

        toolBar = [('Log In', self.user_login, {'side':LEFT}),
                    ('Edit Profile', lambda r=self.request: self.edit_profile(r) , {'side':LEFT}),
                    ('Create department', self.create_department, {'side':LEFT}),
                    ('Create Sector', self.create_sector, {'side':LEFT}),
                    ('Upload Report', self.create_report, {'side':LEFT}),
                    ('Log Out', self.user_logout, {'side':RIGHT})
                    #('Close App', self.quit_, {'side':RIGHT})
                    ]
        self.menuBar = menuBar
        self.toolBar =  toolBar
        self.quickActions = None #quickActions
    def makeWidgets(self):
        ##################
        # context menu
        ##################
        menu_ = Menu(self.master, tearoff=0)
        for L in ['Aaaaaaaaaaaaaaaaa','Bbbbbbbbbbbbbbbbbb','Cccccccccccccccc','Ddddddddddddddd']:
            menu_.add_command(label=L, command=None)
        self.master.bind('<Button-3>', lambda e: menu_.post(e.x_root, e.y_root))
        data = Reports.objects.values()
        Dashboard(self, title="select reports to change", request=self.request,data=data, btn='add report', callback={'add report': self.create_report,'add department': self.create_department, 'add profile': self.edit_profile, 'add sector':self.create_sector})

if __name__ == "__main__":
    App = Tk()
    s = ttk.Style(App)
    s.theme_use('clam')
    s.configure("S.TButton", background="grey", activeforeground="white", relief=RAISED)
    s.configure("L.TButton", background="blue", activebackground="lightblue", activeforeground="white")
    s.configure("C.TFrame", background="red", bd=4)
    s.configure("B.TFrame", background="blue", bd=4)
    s.configure("G.TFrame", background="green", bd=4)
    s.configure("H.TFrame", background="red", bd=4)
    s.configure("R.TButton", background='red', bd=2, relief=FLAT)
    s.configure("B.TButton", background='blue', bd=2, relief=FLAT)
    s.configure("G.TButton", background='green', bd=2, relief=FLAT)
    s.configure("SS.TButton", background='grey', bd=2, relief=FLAT)
    s.configure("H.TLabel", background='white', bd=2, relief=FLAT)
    App.title('Competition Authority of Kenya')
    #App.geometry("1200x800")
    vdesk(App)
    App.mainloop()