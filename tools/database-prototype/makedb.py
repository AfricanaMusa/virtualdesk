#!/usr/bin/env python
'''
###############################################
create create or delete a database 
###############################################
'''

import sys, time
from tkinter import *
from tkinter.messagebox import showinfo, askyesno, showerror
from csvreader import read_csv
from guiStreams import redirectGuiFunc
from conndb import dblogin

def makedb(con,curs,table='product',file=None):
    #print(file)
    if not askyesno('Db','Do you wish to create database with name: ' + file):
        return None
    try:
        names, rows, data  = read_csv(file)
        placeholder = []
        for name in names:
            placeholder.append('%s')
        num = 0
        for row in rows[1:]:
            print('Inserting rows.....' + str(num))
            query = 'insert into {} values ({})'.format(table,",".join(placeholder))
            curs.execute(query,row)
            num += 1
        con.commit()
        return data
    except Exception as er:
        print(er)
        showerror('Error----', er)   
                
    

def makedb_Wrapper(*args,**kwargs):
    result  = redirectGuiFunc(makedb,*args,**kwargs)
    return result

if __name__ == '__main__':
    root = Tk()
    conn, curs = dblogin(dbname='gui_db',user='musa')
    Button(root, text='make database', command= lambda conn=conn, curs=curs: makedb_Wrapper(conn,curs)).pack(fill=X)
    root.mainloop()
    