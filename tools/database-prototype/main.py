#!/usr/bin/env python
'''
##############################
Create Gui window
##############################
'''
import sys
import threading 
from tkinter import *
from tkinter.messagebox import showinfo, showerror
from tkinter.simpledialog import askfloat
from tkinter.filedialog import askopenfilename
from shellgui import ListMenuGui
from scrolledtext import ScrolledText
from makedb import makedb
from conndb import dblogin
from makedicts import makedicts
from guiStreams import redirectGuiFunc, GuiOutput
from grid import SumGrid
from queries import runqueryDialog, searchdlg, insertdlg, deletedlg, updatedlg
from scrolledcanvas import ScrolledCanvas
from query_form import queryDlg

default = 'select * from product'

class AppMain(ListMenuGui):
    def __init__(self, user,dbname):
        self.myMenu = [
            ('Load database', lambda : threading.Thread(target=self.loaddb).start()),
        ]
        self.conn, self.cur = dblogin(user=user, dbname=dbname)
        ListMenuGui.__init__(self)
        t = threading.Thread(target=self.loadrows)
        t.start()
             
    def makeWidgets(self):
        frame = Frame(self)
        frame.pack(expand=YES,fill=BOTH)
        self.frame = frame

        grid = SumGrid(parent=frame, numcol=7)
        grid.config(bd=6, padx=3,pady=4)
        grid.pack(expand=YES,fill=BOTH,side=TOP)
        self.grid_ = grid


        queries_feeback = GuiOutput(frame)
        queries_feeback.text.text.delete('1.0',END)
        queries_feeback.text.pack(expand=YES,fill=BOTH,side=LEFT) 
        queries_feeback.text.text.config(height=10)
        self.queries_feeback = queries_feeback 

        query_container = queryDlg(self, parent=frame)
        query_container.pack(side=RIGHT,expand=YES,fill=BOTH)
        self.query_container = query_container

    def loaddb(self):
        filename = askopenfilename()
        #print(filename)
        if filename:
            print('your here')
            feedback = makedb(self.conn,self.cur,file=filename)
            print(feedback)
            print('your here')
            if feedback:
                self.queries_feeback.write(feedback)
                self.result = redirectGuiFunc(self.makeQuery,self.cur, default)
                if self.result:
                    self.grid_.load_table(self.result)
    
    def makeQuery(self, *args, **kwargs):
        rows = makedicts(*args, **kwargs)
        return rows[:15]
    
    def loadrows(self):
        request = self.makeQuery(self.cur, default)
        if request:
            self.grid_.load_table(request)

    def update_table(self, query='', params=()):
        if not query:
            showinfo('Query Error', 'Please enter a sound query')
        else:
            try:
                self.cur.execute(query,params)
                self.conn.commit()
            except Exception as err:
                showerror('Error',err)
        return self.cur.rowcount


if __name__ == '__main__':
    name = 'Sql Demo'
    AppMain('postgres','gui_db').mainloop()