from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth.models import User
from django.contrib import messages 
from django.http import HttpResponseRedirect
from django.urls import reverse 
from django.views.generic.base import TemplateView, View 
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from .forms import UserRegistrationForm, UserEditForm, ProfileEditForm
from .models import Profile, Contact
# Create your views here.

class register(TemplateView):
    template_name = 'account/register.html'

    def post(self, request, *args, **kwargs):
        user_form = UserRegistrationForm(self.request.POST)
        if user_form.is_valid():
            new_user = user_form.save(commit=False)
            new_user.set_password(user_form.cleaned_data['password'])
            new_user.save()
            profile = Profile.objects.create(user=new_user)
            create_action(new_user, 'has created an account')
            return render(self.request, 'account/register_done.html', {'new_user':new_user,})
    
    def get_context_data(self,**kwargs):
        self.object = UserRegistrationForm()
        context = super().get_context_data(**kwargs)
        context['user_form'] = self.object
        return context

class dashboard(TemplateView):
    template_name = "account/dashboard.html"


    def get_context_data(self,**kwargs):

        try:
            actions = Action.objects.exclude(user=self.request.user)
            following_ids = self.request.user.following.values_list('id',flat=True)
        except:
            following_ids = None
            actions = None
    
        if following_ids:
            # if user is following others, retrieve only thier actions
            actions = actions.filter(user_id__in=following_ids)#.select_related('user','user__profile').prefetch_related('target')
            actions = actions[:10]

        context = super().get_context_data(**kwargs)
        context['section'] = 'dashboard'
        context['actions'] = actions if actions else ''

        return context

class edit(TemplateView):

    template_name = 'account/edit.html'

    def post(self,request, *args, **kwargs):
        user_form = UserEditForm(instance=request.user,data=request.POST)
        profile_form = ProfileEditForm(instance=request.user.profile,data=request.POST,files=request.FILES)

        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Profile updated successfully')
            return HttpResponseRedirect(reverse('dashboard'))
        else:
            messages.error(request, 'Error updating your profile')

    def get_context_data(self, **kwargs):
        self.object = UserEditForm(instance=self.request.user)
        profile_form = ProfileEditForm(instance=self.request.user.profile)
        context = super().get_context_data(**kwargs)
        context['user_form'] = self.object
        context['profile_form'] = profile_form
        return context
class user_list(ListView):
    template_name = 'account/user/list.html'
    context_object_name = 'users'
    model = User
    queryset = User.objects.filter(is_active=True)

class user_detail(DetailView):
    template_name = 'account/user/detail.html'
    context_object_name = 'user'
    model = User

class user_follow(View):

    def post(self, request, *args, **kwargs):
        user_id = request.POST.get('id')
        action = request.POST.get('action')

        if user_id and action:
            try:
                user = User.objects.get(id=user_id)
                if action == 'follow':
                    Contact.objects.get_or_create(user_from=request.user, user_to=user)
                    create_action(request.user, 'is following', user)
                else:
                    Contact.objects.filter(user_from=request.user, user_to=user).delete()
                return JsonResponse({'status':'ok'})
            except User.DoesNotExist:
                return JsonResponse({'status':'ko'})
        return JsonResponse({'status':'ko'})






