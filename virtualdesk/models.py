from django.db import models
from datetime import date
from django.utils import timezone
from django.conf import settings
from django.contrib.auth.models import User

# Create your models here.

class Department(models.Model):
    '''
    stores data of every department in the 
    company
    '''
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=50)
    manager = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL,null=True, blank=True,related_name='manager')
    members = models.ManyToManyField(settings.AUTH_USER_MODEL,blank=True, related_name='members')

    def __str__(self):
        return '{} department'.format(self.name) 

class Sector(models.Model):
    # hold per sector information
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.name}'

class Reports(models.Model):
    '''
    Model for storing all information related to a certain report
    i.e who wrote it when why
    '''
    report_no = models.IntegerField(primary_key=True, unique=True)
    file_title = models.CharField(max_length=50)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name='created_by')
    created_at = models.DateField(default=date.today)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, blank=True)
    sector = models.ForeignKey(Sector, on_delete=models.CASCADE, null=True, blank=True)
    file = models.FileField(upload_to='reports')

    def __str__(self):
        return '{}'.format(self.file_title)

class Profile(models.Model):
    '''
    stores profile for every employee in the company
    '''
    user = models.OneToOneField(settings.AUTH_USER_MODEL,related_name="+", on_delete=models.CASCADE)
    photo = models.FileField(upload_to='profiles', null=True, blank=True)
    date_of_birth = models.DateField(blank=True, null=True)
    department = models.ForeignKey(Department, on_delete=models.CASCADE, null=True, blank=True)
    date_joined = models.DateField(blank=True, null=True)
    gender = models.CharField(max_length=1, choices=(('F','Female'),('M','Male')))
    id_number = models.BigIntegerField(null=True, blank=True)

    def __str__(self):
        return '{}'.format(self.user.username)


    
