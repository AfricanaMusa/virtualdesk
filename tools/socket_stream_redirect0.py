#!/usr/bin/env python
'''
[partial] Tools for connecting streans of non-GUI programs to sockets
that a GUI (or other) can use to interact with non-GUI program;
'''
import sys
from socket import *

port = 50008
host = 'localhost'

def redirectOut(port=port, host=host):
    '''
    connect a caller's standard output stream to a socket for GUI to listen;
    start a caller after listener started, else connect fails before accept
    '''

    sock = socket(AF_INET, SOCK_STREAM)
    sock.connect((host,port))   # caller operates in client mode
    file = sock.makefile('w')   # file interface: text, bufferred
    sys.stdout = file   # make the prints got to sock.end