#!/usr/bin/env python
'''
#####################################################################
 a Python/tkinter text file editor and component.

 Uses the Tk text widget, plus GuiMaker menus and toolbar buttons to
 implement a full featured text editor that can run as a standalone
 program, and attached as a component to other GUIs. 
######################################################################
'''

Version = '2.1'
import sys, os
from tkinter import *
from tkinter.filedialog import Open, SaveAs
from tkinter.messagebox import showinfo, showerror, askyesno
from tkinter.simpledialog import askstring, askinteger
from tkinter.colorchooser import askcolor
from guimaker import *

# general configurations
try:
    import textConfig
    configs = textConfig.__dict__
except:
    configs = {}

START = '1.0'
SEL_FIRST = SEL = '.first'
SEL_LAST = SEL = '.last'

FontScale = 0
if sys.platform[:3] != 'win':
    FontScale = 3

######################################################################################
# Main class: implements editor GUI, actions
# requires a flavor of GuiMaker to be mixed in by more 
# specific subclasses;
# not a direct subclass of GuiMaker because that class takes multiple forms.
######################################################################################

class TextEditor:       # mix with menu?toolbar Frame class
    startfiledir = '.'  # for dialogs
    editwindows = []    # for process wide quit check

    # Unicode configurations
    # imported in class to allow overrides in subclass or self
    if __name__ == '__main__':
        from textConfig import (# My dir is on path
            opensAskUser, opensEncoding, savesUseKnownEcoding, 
            savesAskUser, savesEcoding)
    else:
        from .textConfig import (opensAskUser,opensEncoding,
        savesUseKnownEcoding,savesAskUser, savesEcoding)
    
    ftypes = [('All files', '*'),   # for file open dialog
              ('Text files', '.txt'),  # customize in subclass
              ('Python files', '.py'),] # or set in each instance
                                        
    
    colors = [{'fg':'black', 'bg':'white'},      # color pick list
              {'fg':'yellow', 'bg':'black'},     # first item default
              {'fg':'white', 'bg':'blue'},       # tailor me as desired
              {'fg':'black', 'bg':'beige'},      # or do PickBg/Fg chooser
              {'fg':'yellow', 'bg':'purple'},
              {'fg':'black', 'bg':'brown'},
              {'fg':'lightgreen', 'bg':'darkgreen'},
              {'fg':'darkblue', 'bg':'orange'},
              {'fg':'orange', 'bg':'darkblue'}]
    
    fonts = [('courier', 9+FontScale, 'normal'),
            ('courier', 12+FontScale, 'normal'),
            ('courier', 10+FontScale, 'bold'),
            ('courier', 10+FontScale, 'italic'),
            ('times', 10+FontScale, 'normal'),
            ('helvetica', 10+FontScale, 'normal'),
            ('ariel', 10+FontScale, 'normal'),
            ('system', 10+FontScale, 'normal'),
            ('courier', 10+FontScale, 'normal')]
    
    def __init__(self, loadFirst='', loadEncode=''):
        if not isinstance(self, GuiMaker):
            raise TypeError('TextEditor needs a GuiMaker mixin')
        self.setFileName(None)
        self.lastfind = None
        self.openDialog = None
        self.saveDialog = None
        self.knownEncoding = None
        self.text.focus()
        if loadFirst:
            self.update()
            self.onOpen(loadFirst, loadEncode)
    
    def start(self):
        self.menuBar = [
            ('File', 0,
            [('Open...', 0, self.onOpen),
            ('Save', 0, self.onSave),
            ('Saves As...', 5,self.onSave),
            ('New', 0, self.onNew),
            'separator',
            ('Quit...', 0, self.onQuit)]
            ),

            ('Edit', 0,
            [('Undo', 0, self.onUndo),
            ('Redo', 0, self.onRedo),
            'separator',
            ('Cut', 0,self.onCut),
            ('Copy', 1, self.onCopy),
            ('Paste', 0, self.onPaste),
            'separator',
            ('Delete', 0, self.onDelete),
            ('SelectAll', 0, self.onSelectAll)]
            ),

            ('Search', 0,
            [('Goto', 0, self.onGoto),
            ('Find', 0, self.onFind),
            ('Refind', 0, self.onRefind),
            ('Change', 0, self.onFind),
            ('Grep', 0, self.onGrep)]
            ),

            ('Tools', 0,
            [('Pick Font', 6, self.onPickFont),
            ('Font List', 0, self.onFontList),
            'separator',
            ('Pick Bg', 3,self.onPickBg),
            ('Pick Fg', 0, self.onPickFg),
            ('Color List', 0, self.onColorList),
            'separator',
            ('Info', 0, self.onInfo),
            ('Clone', 1, self.onClone),
            ('Run Code', 0, self.onRunCode)]
            )]
        self.toolBar = [
            ('Save', self.onSave, {'side': LEFT}),
            ('Cut', self.onCut, {'side': LEFT}),
            ('Copy', self.onCopy, {'side': LEFT}),
            ('Paste', self.onPaste, {'side': LEFT}),
            ('Find', self.onRefind, {'side': LEFT}),
            ('Help', self.help, {'side': RIGHT}),
            ('Quit', self.onQuit, {'side': RIGHT})]
    
    def makeWidgets(self):          # run by GuiMaker.__init__
        name = Label(self, bg='black', fg='white')  # add below menu, above tool
        name.pack(side=TOP, fill=X)     # menu/toolbars are packed

        vbar = Scrollbar(self)
        hbar= Scrollbar(self,orient='horizontal')
        text = Text(self, padx=5, wrap='none') 
        text.config(undo=1, autoseparators=1)


        vbar.pack(side=RIGHT, fill=Y)
        hbar.pack(side=BOTTOM, fill=X)  # pack text last
        text.pack(side=TOP, fill=BOTH, expand=YES)  #else sbars clipped

        text.config(yscrollcommand=vbar.set) #call vbar.set text move
        text.config(xscrollcommand=hbar.set)

        vbar.config(command=text.yview) # call text.yview on scrool move
        hbar.config(command=text.xview) # or hbar['command]=text.xview

        startfont = configs.get('font', self.fonts[0])
        startbg = configs.get('bg', self.colors[0]['bg'])
        startfg = configs.get('fg', self.colors[0]['fg'])
        text.config(font=startfont, bg=startbg, fg=startfg)
        if 'height' in configs: text.config (height=configs['height'])
        if 'width'  in configs: text.config(width=configs['width'])
        self.text = text
        self.filelabel = name

##################################################################################
# File menu commands
##################################################################################

    def my_askopenfilename(self):   # objects remember last result dir/file
        if not self.openDialog:
            self.openDialog = Open(initialdir=self.startfiledir,
                                    filetypes=self.ftypes)
        return self.openDialog.show()
    
    def my_asksaveasfilename(self): # objects remember last result dir/file
        if not self.saveDialog:
            self.saveDialog = SaveAs(initialdir=self.startfiledir,
                                    filetypes=self.ftypes)
        return self.saveDialog.show()
    
    def onOpen(self, loadFirst='', loadEncode=''):
        '''
            Open in text mode with an encoding passed in, input from the user,
            in textConfig, or platform default, or open as bytes  arbitrary Unicode 
            encodings as last resort and drop \r in Windows as end-lines if present
            so text  displays normally; content fetches are returned
            as try, so no need to encode save: keep encoding used here

            test if file is okey ahead of time to try to avoid opens;
            we could also load and manually decode bytes to str to avoid multiple open attempts, 
            but this is unlikely to try all cases;

            encoding behaviour is configurable in the local textCOnfig.py:
            1) tries known type first if passed in by client(email charsets)
            2) if openAskUser True, try input next (prefill with defaults)
            3) if opensEncoding nonempty, try this encoding next: 'latin-1',etc.
            4) tries sys.getdefaultencoding() platform default next
            5) uses binary mode bytes and Tk policy as the last resort

        '''
        if self.text_edit_modified():
            if not askyesno('PyEdit', 'Text has changed: discard changes?'):
                return
        file = loadFirst or self.my_askopenfilename()
        if not file:
            return
        if not os.path.isfile(file):
            showerror('PyEdit', 'Could not open file' + file)
            return
        # try known encoding if passed and accurate(e.g ..email)
        text = None # empty file = '' = False: test for None!
        if loadEncode:
            try:
                text = open(file, 'r', encoding=loadEncode).read()
                self.knownEcoding = loadEncode
            except (UnicodeError, LookupError, IOError):
                pass
        # try config file (or before ask user?)
        if text == None and self.opensEncoding:
            try:
                text = open(file, 'r', encoding=self.opensEncoding).read()
                self.knownEncoding = self.opensEncoding
            except (UnicodeError, LookupError, IOError):
                pass
        # try platform default (utf-8 on windows; try utf8 always?)
        if text == None:
            try:
                text = open(file, 'r', encoding=sys.getdefaultencoding()).read()
                self.knownEncoding = sys.getdefaultencoding()
            except (UnicodeError, LookupError, IOError):
                pass
        # last resort: use binary bytes and rely on Tk to decode
        if text == None:
            try:
                text = open(file, 'rb').read()
                text = text.replace(b'\r\n',b'\n')
                self.knownEncoding = None
            except IOError:
                pass
        if text == None:
            showerror('PyEdit', 'Could not decode and open file' + file)
        else:
            self.setAllText(text)
            self.setFileName(file)
            self.text.edit_reset()      # clear undo/redo stks
            self.text.edit_modified(0)  # clear modified flag
    
    def onSave(self):
        self.onSaveAs(self.currfile) # maybe None
    
    def onSaveAs(self,forcefile=None):
        '''
        Text content is always returned as str, so we must deal with encodings to saves to
        a file here, regardless of open mode of the output file (binary
        requires bytes, and text must encode); tries the encoding used when opened or saved (if known).
        user input, conifg file setting, and platform default last; most users can use platform default;

        retains sucessful encoding name here to next save, because this maybe the first Save after 
        New or manual text insertion; Save and Saveas may bith use last known encoding, per config file
        (it probably should be used for Save, but SaveAs is unclear);
        gui prompts are prfilled with the known encoding if there is one;

        does manual text.encode() to avoid creating file; text mode files
        perform platform specific end-line conversion: windows \r dropped
        if present on open by text mode (auto) and binary mode (manually)
        if manual content inserts, must delete \r else dulicates here;
        knowEcoding=None before first Open or Save, after New, if binary Open

        encoding behavious is configurable in the local textConfig.py:
        1) if savesUseKnownEncoding > 0, try encoding from last open or save
        2) if savesAskUser True, try user input next (prefill with known?)
        3) if savesEncoding nonempty, try this encoding next: 'utf-8', etc
        $ tries sys.getdefaultencoding() as a last resort
        '''

        filename = forcefile or self.my_asksaveasfilename()
        if not filename:
            return
        text = self.getAllText()    # a str string, with \n eolns,
        encpick = None        # even if read/inserted as bytes

        # try known encoding at last open or save
        # latest Open or Save, if an
        if self.knownEncoding and (
            (forcefile and self.savesUseKnownEcoding >=1 ) or
            ( not forcefile and self.savesUseKnownEcoding >=2 )):
            try:
                text.encode(self.knownEncoding)
                encpick = self.knownEncoding
            except UnicodeError:
                pass
        
        #try user input, prefill with known type, else next choice
        if not encpick and self.savesAskUser:
            askuser = askstring('PyEdit', 'Enter Unicode encoding for save',
                                initialvalue=(self.knownEncoding or self.savesEcoding or self.getdefaultencoding() or ''))
            
            if askuser:
                try:
                    text.encode(askuser)
                    enpick = askuser
                except (UnicodeError, LookupError): # bad name
                    pass        # UnicodeError: cant encode
        # try config file
        if not encpick and self.savesEcoding:
            try:
                text.encode(self.savesEcoding)
                encpick = self.savesEcoding
            except (UnicodeError, LookupError):
                pass
        
        # try platform default (uf8 on windows)
        if not encpick:
            try:
                text.encode(sys.getdefaultencoding())
                encpick = sys.getdefaultencoding()
            except (UnicodeError, LookupError):
                pass
        
        #open in text mode for endlines + encoding
        if not enpick:
            showerror('PyEdit', 'Could not encode for file' + filename)
        else:
            try:
                file = open(filename, 'w', encoding=encpick)
                file.write(text)
                file.close()
            except:
                showerror('PyEdit', 'Could not write file' + filename)
            else:
                self.setFileName(filename)
                self.text.edit_modified(0)
                self.knownEncoding = encpick
    
    def onNew(self):
        '''
        start editing a new file from scratch in current window;
        see on Clone to pop-up a new edit window instead;
        '''
        if self.text_edit_modified():
            if not askyesno('PyEdit', 'Text has changed: discard changes?'):
                return
        self.setFileName(None)
        self.clearAllText() 
        self.text.edit_reset()  # clear undo/redo stks
        self.text_edit_modified(0) # clear modified flag
        self.knownEncoding = None # Unicode type unknown
    
    def onQuit(self):
        '''
        on Quit menu/toolbar select and wm border X button in toplevel windows;
        don't exit app if other changed; don't ask if self unchanged;
        moved to the top-level window classes at the end since may vary per usage:
        a Quit in GUI muight quit() to exit, destroy just one TopLevel, Tk, or 
        edit frame, or not provided at all when run as an attached component;
        check self for changes, and if might quit(), main windows should 
        check other windows in the process-wide list to ee if they have changed too;
        '''
        assert False, 'onQUIt must be defined in window-specific subclass'
    
    def text_edit_modified(self):
        return self.text.edit_modified()


#####################################################################################
# Edit menu commands
#####################################################################################

    def onUndo(self):
        try:
            self.text.edit_undo()   # keeps undo/redo stacks
        except TclError:
            showinfo('Pyedit', 'Nothing to undo')
    
    def onRedo(self):
        try:
            self.text.edit_redo()
        except TclError:
            showerror('PyEdit', 'Nothing to redo')
    
    def onCopy(self):
        if not self.text.tag_ranges(SEL):   # get text selected by mouse etc.
            showerror('PyEdit', 'No text selected')
        else:
            text = self.text.get(SEL_FIRST, SEL_LAST)
            self.clipboard_clear()
            self.clipboard_append(text)
    
    def onDelete(self):     # delete selected text, no save
        if not self.text.tag_ranges(SEL):
            showerror('PyEdit', 'No text selected')
        else:
            self.text.delete(SEL_FIRST, SEL_LAST)
    
    def onCut(self):
        if not self.text.tag_ranges(SEL):
            showerror('PyEdit', 'No text selected')
        else:
            self.onCopy()
            self.onDelete()
    
    def onPaste(self):
        try:
            text = self.selection_get(selection='CLIPBOARD')
        except TclError:
            showerror('PyEdit', 'Nothing to paste')
            return
        self.text.insert(INSERT, text)  # add att current cursor 
        self.text.tag_remove(SEL, '1.0', END)
        self.text.tag_add(SEL, INSERT+'-%dc' % len(text), INSERT)
        self.text.see(INSERT)   # select it, so it can be cut
    
    def onSelectAll(self):
        self.text.tag_add(SEL, '1.0', END+'-1C')    # select entire text
        self.text.mark_set(INSERT, '1.0')   # move insert point to top
        self.text.see(INSERT)           # scroll to top

###############################################################################
# Search Menu Commands
###############################################################################

    def onGoto(self, forceline=None):
        line = forceline or askinteger('PyEdit', 'Enter line number')
        self.text.update()
        self.text.focus()
        if line is not None:
            maxindex = self.text.index(END+'-1c')
            maxline = int(maxindex.split('.')[0])
            if line > 0 and line <= maxline:
                self.text.mark_set(INSERT, '%d.0' % line)  # goto line
                self.text.tag_remove(SEL, '1.0', END)   # delete selects
                self.text.tag_add(SEL, INSERT, 'insert + 1l')   # select line
                self.text.see(INSERT)
            else:
                showerror('PyEdit','Bad line number')
    
    def onFind(self, lastkey=None):
        key = lastkey or askstring('PyEdit', 'Enter search string')
        self.text.update()
        self.text.focus()
        self.lastfind = key

        if key:
            nocase = configs.get('caseinsens', True)
            where  = self.text.search(key, INSERT, END, nocase=nocase)
            if not where:
                showerror('PyEdit', 'String not found')
            else:
                pastkey = where + '+%dc' % len(key) # index past key
                self.text.tag_remove(SEL, '1.0', END)   # remove any sel
                self.text.tag_add(SEL, where, pastkey)  # select key
                self.text.mark_set(INSERT, pastkey) # for next find
                self.text.see(where)        # scroll display
    
    def onRefind(self):
        self.onFind(self.lastfind)
    
    def onChange(self):
        '''
        non-modal find/change dialog
        '''
        new = Toplevel(self)
        new.title('PyEdit - change')
        Label(new, text='Find text?', relief=RIDGE, width=15).grind(row=0, column=0)
        Label(new, text='Change to?', relief=RIDGE, width=15).grid(row=1, column=0)
        entry1 = Entry(new)
        entry2 = Entry(new)
        entry1.grid(row=0, column=1, sticky=EW)
        entry2.grid(row=1, column=1, sticky=EW)

        def onFind():
            self.onFind(entry1.get())   # runs normal find dialog callback
        
        def onApply():
            self.onDoChange(entry1.get(), entry2.get())
        
        Button(new, text='Find', command=onFind).grind(row=0, colum=2, sticky=EW)
        Button(new, text='Apply', command=onApply).grid(row=1, column=2, sticky=EW)
        new.columnconfigure(1, weight=1)

    def onDoChange(self, findtext, changeto):
        # on Apply in change dialog: change and refind
        if self.text.tag_ranges(SEL):
            self.text.delete(SEL_FIRST, SEL_LAST)
            self.text.insert(INSERT, changeto)  # delete if empty
            self.text.see(INSERT)
            self.onFind(findtext)   # goto next apper
            self.text.update()  # force refresh
    
    def onGrep(self):
        '''
        threaded file search;
        search matched filenames in directory tree for string;
        listbox clicks open matched file at line of occurrence;

        search is threaded so the GUI remains active and is not blocked
        and to allow multiple greps to overlap in time;
        could use threadtools, but avoid loop in no active grep;
        '''
        from formrows import makeFormRow

        # nonmodal dialog: get dirname, filenamepatt, grepkey
        popup = Toplevel()
        popup.title('PyEdit -grep')
        var1 = makeFormRow(popup, label='Direcotry root', width=18, browse=False)
        var2 = makeFormRow(popup, label='Filename pattern', width=18, browse=False)
        var3 = makeFormRow(popup, label='Search string', width=18, browse=False)
        var4 = makeFormRow(popup, label='Content encoding', width=18, browse=False)

        var1.set('.')   # current dir
        var2.set('*.py')    #initial values
        var4.set(sys.getdefaultencoding())  # for file content, not filenames
        cb = lambda: self.onDoGrep(var1.get(), var2.get(), var3.get(), var4.get())
        Button(popup, text='GO!', command=cb).pack()
    
    def onDoGrep(self, dirname, filenamepatt, grepkey, encoding):
        '''
        on Go in dialog: populate scrolled list with matches TBD: should producer be
        a daemon so it dies with app?
        '''
        import threading, queue

        # make non-modal un-closeable dialog
        mypopup = Tk()
        mypopup.title('PeEdit = grepping')
        status = Label(mypopup, text='Grep thread searching for: %r..' % grepkey)
        mypopup.protocol('WM_DELETE_WINDOW', lambda: None) # ingone X close

        # start producer thread, consumer loop
        myqueue = queue.Queue()
        threadsargs = (filenamepatt, dirname, grepkey, encoding, myqueue)
        threading.Thread(target=self.greoThreadProducer, args=threadargs).start()
        self.grepThreadConsumer(grepkey, encoding, myqueue, mypopup)
    
    def grepThreadProducer(self, filenamepatt, dirname, grepkey, encoding, myqueue):
        '''
        in a non-GUI parallel thread: queue find.find results results list;
        could also queue matches as found, but need to keep window;
        file content and file names may both faiil to decode
        '''
        pass
    def grepThreadConsumer(self,grepkey, encoding, myqueue, mypopup):
        '''
        in the main GUI thread: watch for results or [];
        there may be multiple active grep threads/loops/queues;
        '''
        pass
    def grepMatchesList(self, grepkey, encoding):
        '''
        populate list after sucessful matches;
        '''
        pass

############################################################################
# Tools menu commands
############################################################################

    def onFontList(self):
        self.fonts.append(self.fonts[0]) # pick next font on the list
        del self.fonts[0]       # resizez the text area
        self.text.config(font=self.fonts[0])
    
    def onColorList(self):
        self.colors.append(self.colors[0])  #pick color from list
        del self.colors[0]              # move current to end
        self.text.config(gf=self.colors['fg'], bg=self.color['bg'])
    
    def onPickFg(self):
        self.pickColor('fg')

    def onPickBg(self):
        self.pickColor('bg')

    def pickColor(self, part):
        (triple, hexstr) = askcolor()
        if hexstr:
            self.text.config(**{part:hexstr})
    
    def onInfo(self):
        '''
        pop-up dialog giving statistics and cursor location
        caveat, Tk insert position column counts a tab as one
        character: trabslate to next multiple of 8 to match visual?
        '''

        text = self.getAllText()
        byts = len(text)
        lines = len(text.split('\n'))
        words = len(text.split())
        index = self.text.index(INSERT)
        where = tuple(index.split('.'))
        showinfo('PyEdit Information',
                 'Current location: \n\n' +
                 'line:\t%s\ncolumn:\t%s\n\n' % where +
                 'File text statistics:\n\n' +
                 'chars:\t%d\nlines:\t%d\nwords:\t%d\n' % (byts, lines, words))
    
    def onClone(self, makewindow=True):
        '''
        open a new edit window without changing one already open (onNew);
        inherits quit and other behavior of the window that it clones;
        subclass must redefines/replace this if its own popup
        else this create a bogus extra window here which will be empty
        '''

        if not makewindow:
            new  = None
        else:
            new = Toplevel()
        myclass = self.__class__
        myclass(new)
    
    def onRunCode(self, parallelmode=True):
        '''
        run python code being editied--not an IDE, but handy;
        tries to run in files's dir, not cwd; inputs and adds command-line arguments for scripts files;
        '''
        def askcmdargs():
            return askstring('PyEdit', 'Commandline arguments?') or ''
        
        from launchmodes import System, Start, StartArgs, Fork

        filemode = False
        thefile = str(self.getFileName())
        if os.path.exists(thefile):
            filemode = askyesno('PyEdit', 'Run from file')
            self.update()
        if not filemode:
            cmdargs = askcmdargs()
            namespace = {'__name__':'__main__'}
            sys.argv = [thefile] + cmdargs.split()
            exec(self.getAllText() + '\n', namespace)
        elif self.text_edit_modified():
            showerror('PyEdit', 'Text changed: you must save before run')
        else:
            cmdargs = askcmdargs()
            mycwd = os.getcwd()
            dirname, filename = os.path.split(thefile)
            os.chdir(dirname or mycwd)
            thecmd = filename + ' ' + cmdargs
            if not parallelmode:
                System(thecmd, thecmd)()
            else:
                if sys.platform[:3] == 'win':
                    run = StartArgs if cmdargs else Start
                    run(thecmd, thecmd)()
                else:
                    Fork(thecmd, thecmd)()
            os.chdir(mycwd)
    
    def onPickFont(self):
        '''
        non-modal font spec dialog
        pass per-dialog to callback, may be > 1 font dialog open
        '''
        from formrows import makeFormRow
        popup = Toplevel(self)
        popup.title('PyEdit - font')
        var1 = makeFormRow(popup, label='Family', browse=False)
        var2 = makeFormRow(popup, label='Size', browse=False)
        var3 = makeFormRow(popup, label= 'Style', browse=False)
        var1.set('courier')
        var2.set('12')
        var3.set('bold italic')
        Button(popup, text='Apply', command=
                                            lambda:self.onDoFont(var1.get(), var2.get(), var3.get())).pack()
    
    def onDoFont(self,Family, size, style):
        try:
            self.text.config(font=(family, int(size), style))
        except:
            showerror('PyEdit', 'Bad font specification')
            
##################################################################################
# Utilities, useful outside this class
##################################################################################

    def isEmpty(self):
        return not self.getAllText()

    def getAllText(self):
        return self.text.get('1.0', END+'-1c') # extract text from str string
    
    def setAllText(self, text):
        '''
        caller call selfupdate() first if just packed, else thse initial position may be
        line 2, not line 1
        '''
        self.text.delete('1.0', END)
        self.text.insert(END, text)
        self.text.mark_set(INSERT, '1.0')
        self.text.see(INSERT)
    
    def clearAllText(self):
        self.text.delete('1.0', END)
    
    def getFileName(self):
        return self.currfile
    
    def setFileName(self, name):
        self.currfile = name # for save
        self.filelabel.config(text=str(name))
    
    def setKnownEncoding(self, encoding='utf-8'):
        self.knowEcoding = encoding
    
    def setBg(self, color):
        self.text.config(bg=color)

    def setFg(self, color):
        self.text.config(fg=color)

    def setFont(self, font):
        self.text.config(font=font)
    
    def clearModified(self):
        self.text.edit_modified(0)
    
    def isModified(self):
        return self.text_edit_modified()
    
    def help(self):
        showinfo('About PyEdit', helptext % ((Version,)*2))

######################################################################################################
# Ready-to-use editior classes
# mixes in GuiMaker Frame subclass which builds menu and toolbars
#
# these classes are common use cases, but other configurations are possible;
# class TextEditor().mainloop() to start PyEdit as a standalone program;
# redefine/extend onQuit in a subclass to catch exit or destory 
######################################################################################################

class TextEditorMain(TextEditor, GuiMakerWindowMenu):
    '''
    main PyEdit windows that quit() to exit app on a QUit in Gui, and build a menu on a window; parent may be default
    Tl, explicit Tk, or Toplevel parent must be a window,, and probaly should be Tk so isn't silently destoryed and closed with parent
    ; all main PyEdit windows check all other PyEdit window open in the process for cahnges on a quit in the GUI
    '''

    def __init__(self, parent=None, loadFirst='', loadEncode=''):
        # editor fills whole parent window
        GuiMaker.__init__(self, parent) # use main window menus
        TextEditor.__init__(self, loadFirst, loadEncode) # GUIMaker frame packs self
        self.master.title('PyEdit' + Version)
        self.master.iconname('PyEdit')
        self.master.protocol('WM_DELETE_WINDOW', self.onQuit)
        TextEditor.editwindows.append(self)
    
    def onQuit(self):
        close = not self.text_edit_modified()
        if not close:
            close = askyesno('PyEdit', 'Text changed: quit and discard changes?')
        if close:
            windows = TextEditor.editwindows
            changed = [w for w in windows if w != self and w.text_edit_modified()]
            if not changed:
                GuiMaker.quit(self) # quit ends entire app regardless of the widget type
            else:
                numchange = len(changed)
                verify = '%s other edit window%s changed: quit and discard anyhow?'
                verify = verify % (numchange, 's' if numchange > 1 else '')
                if askyesno('PyEdit', verify):
                    GuiMaker.quit(self)

class textEditorMainPopup(TextEditor, GuiMakerWindowMenu):
    ''' 
    popup PyEdit windows that destory() to close only on self a QUIt in GUI,
    and build a menu on a window; makes own Toplevel parent, which is  child
    to default Tk (for None) or other passed-in window or widget (e.g, a frame);
    adds to list to will be checked for changes if any PyEit main window quits;
    if any PyEdit main windows will be created, parent of this should also be a PyEdit main window's
    parent so this is not closed silently while being tracked;
    OnQUit is run for QUit in toolbar or File menu, as well as window border x'
    '''

    def __init__(self, parent=None, loadFirst='', winTitle='', loadEncode=''):
        # create own window
        self.popup = Toplevel(parent)
        GuiMaker.__init__(self, self.popup)
        TextEditor.__init__(self, loadFirst, loadEncode)
        assert self.master == self.popup
        self.popup.title('PyEdit' + Version + winTitle)
        self.popup.iconname('PyEdit')
        self.popup.protocol('WM_DELETE_WINDOW', self.onQuit)
        TextEditor.editwindows.append(self)

    def OnClone(self):
        TextEditor.onClone(self, makewindow=False) # i make my own pop-up


############################################################################
# when editor is embedded in another window
############################################################################

class TextEditorComponent(TextEditor, GuiMakerFrameMenu):
    '''
    attched PyEdit compenent frames with full menu/toolbar opotions,
    which run a destroy on Quit in the GUI to erase self only;
    a Quit in the GUI verifies if any changes in self (only) here;
    does not intercept window manager border X: doesn't own widnow;
    does not add self to changes tracking list: part of larger app;
    '''

    def __init__(self, parent=None, loadFirst='', loadEncode=''):
        # use Frame-based menus
        GuiMaker.__init__(self, parent)     # GuiMaker must init first
        TextEditor.__init__(self, loadFirst, loadEncode)
    
    def onQuit(self):
        close = not self.text_edit_modified()
        if not close:
            close = askyesno('PyEdit', 'Text changed: quit and discard changes?')
        if close:
            self.destroy() # erase self Frame but do not quit enclosing app

class TextEditorComponentMinimal(TextEditor, GuiMakerFrameMenu):
    '''
    attached PyEdit component frames without Quit and File menu options;
    on startup, removes Quit from toolbar, and either deletes FIle menu
    or disables all it's items 
    Menu and toolbars structures are per-instance data: changes do not impact others;
    Quit in GUI never occurs, because it's removed from available options;
    '''

    def __init__(self, parent=None, loadFirst='', deleleFile=True, loadEncode=''):
        self.deleteFile = deleleFile
        GuiMaker.__init__(self, parent) # GUI maker frames packs itself
        TextEditor.__init__(self, loadFirst, loadEncode) # TextEditor adds middle

    def start(self):
        TextEditor.start(self)
        for i in range(len(self.toolBar)):
            if self.toolBar[i][0] == 'Quit':
                del self.toolBar[i]
                break
        if self.deleteFile:
            for i in range(len(self.menulBar)):
                if self.toolBar[i][0] == 'File':
                    del self.toolBar[i]
                    break
        else:
            for (name, key, items) in sell.menuBar:
                if name == 'File':
                    items.append([1,2,3,4,6])

############################################################################################
# standalone program run
############################################################################################

def testPopup():
    root = Tk()
    textEditorMainPopup(root)
    TextEditorMainPopup(root)
    Button(root, text='More', command=textEditorMainPopup).pack(fill=X)
    Button(root, text='QUit', command=root.quit).pack(fill=X)

def main():
    try:
        fname = sys.argv[1]
    except IndexError:
        fname = None
    TextEditorMain(loadFirst=fname).pack(expand=YES, fill=BOTH) # pack optional
    mainloop()

if __name__ == "__main__":
    #testPopup()
    main()
        





















        







            
