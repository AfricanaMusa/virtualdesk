#!/usr/bin/env python
# GUI that displays data produces and queued by worker threads (class-based)

import threading, queue, time
from tkinter.scrolledtext import ScrolledText

class ThreadGui(ScrolledText):
    def __init__(self,parent=None):
        ScrolledText.__init__(self,parent)
        self.threads = 4
        self.pack()
        self.dataQueue = queue.Queue()
        self.bind('<Button-1>', self.makethreads)
        self.consumer()

    def producer(self,id):
        for i in range(5):
            time.sleep(0.1)
            self.dataQueue.put('[Producer id=%d, count=%d]' % (id, i))
    
    def consumer(self):
        try:
            data = self.dataQueue.get(block=False)
        except queue.Empty:
            pass
        else:
            self.insert('end', 'consumer got => %s\n' % str(data))
            self.see('end')
        self.after(100,self.consumer)

    def makethreads(self,event):
        for i in range(self.threads):
            threading.Thread(target=self.producer, args=(i,)).start()

if __name__ == "__main__":
    root = ThreadGui()
    root.mainloop()