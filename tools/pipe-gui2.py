#!/usr/bin/env python
# GUI reader side: like pipe-gui1, but make root window and mainloop explicit

from tkinter import *
from guiStreams import redirectedGuiShellCmd

def launch():
    redirectedGuiShellCmd('python -u pipe-nongui.py')

window = Tk()
Button(window, text='GO!', command=launch).pack()
window.mainloop()