#!/usr/bin/env python
'''
PyEdit (textEditor.py) user startup configuration module;
'''

#------------------------------------------------------------------------------------
# General configurations
# comment-out any setting in this section to accept Tk or programs 
# can also change font/colors from GUI menus, and resize window when open;
# import via search path: can be define per client app, skipped if not on the path
#-------------------------------------------------------------------------------------

#initial font
font = ('courier', 9, 'normal')

#initial color              # default= white, black
bg = 'lightcyan'            #color name of RGB gexstr
fg = 'black'                

#initial size       
height = 20         # Tk default: 24 lines
width = 80          # Tk default: 80 characters

# search case-insensitive
caseinsens = True           # default=1/True(on)

#------------------------------------------------------------------------
# Unicode encoding behavior and names for file opens and saves;
# attemps the cases listed belowe in the order shown, until the first one
# that works; set all variables to false/empty/0 to use your platform's
# default (which is 'utf-8') saveUseKnowEncoding: 0=No, 1=Yes, for Save
# only, 2=Yes for Save and SaveAs imported from this file always: sys.path
# iifmain, else package relative
#---------------------------------------------------------------------------------

                        # 1) tries internally known type first 
opensAskUser = True     # 2) if True, try user input next (prefill with defaults)
opensEncoding = ''      # 3) if nonempty, try this encoding next: 'latin-1'
                        # 4) tries sys.getdefaultencodiing() platform default next
                        # 5) uses binary modes bytes and Tk policay as the last resort

savesUseKnownEcoding = 1 # 1) if > 0, try known encoding from last open or save
savesAskUser = True       # 2) if True, try user input next (prefill with known?)
savesEcoding = ''         # 3) if nonempty, try this encoding next: 'utf-8', etc
                          # 4) tries sys.getdefaultencoding() as a last resort
