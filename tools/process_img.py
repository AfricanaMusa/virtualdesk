#!/usr/bin/env python
import os
from PIL import Image as Mage
from PIL import ImageTk
from tkinter import *
from tkinter import ttk

def pImage(parent, imgdir, imgfile, subdir, size=(1200,350)):
    '''
    open image and create thumbnail
    '''
    lbl = ttk.Label(parent)
    thumbdir = os.path.join(imgdir, subdir)
    if not os.path.exists(thumbdir):
        os.mkdir(thumbdir)
    thumbpath = os.path.join(thumbdir, imgfile)
    if os.path.exists(thumbpath):
        thumbobj = Mage.open(thumbpath)
        img = ImageTk.PhotoImage(thumbobj)
        lbl.config(image=img)
        lbl.image = img
        return lbl
    else:
        imgpath = os.path.join(imgdir, imgfile)
        imgobj = Mage.open(imgpath)
        width, height = imgobj.size
        imgobj.thumbnail(size)
        imgobj.save(thumbpath)
        img = ImageTk.PhotoImage(imgobj)
        lbl.config(image=img)
        lbl.image = img
        return lbl
