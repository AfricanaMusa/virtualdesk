#!/usr/bin/env python
'''
profile edit form
'''

from tkinter import *
from tkinter import ttk
from tkinter.messagebox import *
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from virtualdesk.models import Profile
from tools.formrows import *
from tools.process_img import pImage
from tools.widgets import cframe, ccframe

class editProfile(Toplevel):
    '''
    form for editing user object and user profile 
    '''
    def __init__(self,parent=None, request=None, image_dir=None, subdir=None):
        Toplevel.__init__(self,parent)
        self.parent = parent
        self.request = request
        self.image_dir = image_dir
        self.subdir = subdir
        self.transient(parent)
        self.makewidgets()
        self.grab_set()
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        self.initial_focus = self
        if not (self.initial_focus):
            self.initial_focus = self
        self.initial_focus.focus_set()
        self.wait_window(self)
    
    def makewidgets(self):
        ############
        # containers
        ############

        container = ccframe(self, "LOG IN", "G.TFrame", "G.TButton")
        photo_container = ttk.Frame(container.body, width=100)
        lbl = pImage(photo_container, self.image_dir,'profile-pic.jpg', self.subdir)
        lbl.pack(fill=BOTH)
        user_container = Frame(container.body)
        user_container.configure(background='white')
        profile_container = Frame(container.body)
        profile_container.configure(background='white')
        btns_container = Frame(container.body)
        btns_container.configure(background='white')

        ####################
        # forms
        ####################
        self.first_name = makeFormRowString(user_container, 'first name'.upper(), browse=False, focus=True)
        self.last_name = makeFormRowString(user_container, 'last name'.upper(), browse=False)
        self.email = makeFormRowString(user_container, 'email'.upper(), browse=False)
        self.photo = makeFormRowString(profile_container, 'user profile'.upper())
        self.date_of_birth = makeFormRowString(profile_container, 'date of birth'.upper(), browse=False)
        self.department= makeFormRowString(profile_container, 'department'.upper(), browse=False)
        self.date_joined = makeFormRowDate(profile_container, 'date joined'.upper(), browse=False)
        self.gender = makeFormRowString(profile_container, 'gender'.upper(), browse=False)
        self.id_number = makeFormRowString(profile_container, 'id number'.upper(), browse=False)

        ####################
        # bind event
        ####################
        self.bind('<Return>', self.profile_update)
        self.bind('<Escape>', lambda event:self.destroy())

        ###################
        # action buttons
        ###################

        update = ttk.Button(btns_container, text='upload'.upper(), style="L.TButton", command=self.profile_update)
        cancel = ttk.Button(btns_container, text='clear'.upper(), style="S.TButton")
        close = ttk.Button(btns_container, text='exit'.upper(), command=self.destroy, style="S.TButton")

        #####################
        # config and packing
        #####################
        photo_container.pack(expand=YES, fill=BOTH,side=LEFT)
        user_container.pack(expand=YES, fill=BOTH, padx=5)
        profile_container.pack(expand=YES, fill=BOTH, padx=5)

        close.pack(side=RIGHT, fill=X, padx=4,pady=4)
        cancel.pack(side=RIGHT, fill=X, padx=4,pady=4)
        update.pack(side=RIGHT, fill=X, padx=4,pady=4)

        btns_container.pack(expand=YES, fill=BOTH, padx=5)
        
        container.pack(fill=BOTH, expand=YES, padx=5, pady=5)

    def profile_update(self, event=None):
        '''
        update user and profile table
        '''
        user = User.objects.get(pk = self.request.user.id)
        if user:
            if self.first_name.get() != 'first name'.upper():
                user.first_name = self.first_name.get()
            if self.last_name.get() != 'last name'.upper():
                user.last_name = self.first_name.get()
            if self.email.get() != 'email'.upper():
                user.first_name = self.first_name.get()
            user.save()
            profile = Profile.objects.get(user__exact=self.request.user.id)
            if profile:
                profile.user = user
                if self.photo.get() != 'user profile'.upper():
                    profile.photo = self.photo.get()
                if self.date_of_birth.get() != 'date of birth'.upper():
                    profile.date_of_birth = self.date_of_birth.get()
                if self.department.get() != 'department'.upper():
                    profile.department= self.department.get()
                if self.date_joined.current_date != 'date joined'.upper():
                    profile.date_joined = self.date_joined.current_date
                if self.gender.get() != 'gender'.upper():
                    profile.gender = self.gender.get()
                if self.id_number.get() != 'id number'.upper():
                    profile.id_number = self.id_number.get()
                profile.save()
                showinfo("Profile",' profile sucessfully updated')
            else:
                p = Profile(
                    user=user,
                    photo =  self.photo.get() if  self.photo.get() != 'user profile'.upper() else None,
                    date_of_birth =  self.date_of_birth.get() if  self.date_of_birth.get() != 'date of birth'.upper() else None,
                    department =  self.department.get() if  self.department.get() != 'department'.upper() else None,
                    date_joined =  self.date_joined.current_date if  self.date_joined.current_date != 'date joined'.upper() else None,
                    gender =  self.gender.get() if  self.gender.get() != 'gender'.upper() else None,
                    id_number =  self.id_number.get() if  self.id_number.get() != 'id number'.upper() else None,
                )
                p.save()
                showinfo("Profile", "Profile sucessfully created.")
                
                

if __name__ == "__main__":
    editProfile().mainloop()

