#!/usr/bin/env python
# non-gui side: proceed normally, no need for special code

import time
while True:
    print(time.asctime())
    time.sleep(2.0)