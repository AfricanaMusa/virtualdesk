#!/usr/bin/env python
'''
create login dialog
'''
from tkinter import *
from tkinter import ttk
from tkinter.messagebox import *
from django.contrib.auth import authenticate, login, logout
from django.http.request import HttpRequest
from django.conf import settings
from django.utils.module_loading import import_module
from tools.formrows import *
from tools.widgets import cframe, ccframe
from tools.process_img import pImage
from register_user import registerUser


class loginDialog(Toplevel):
    '''
    create toplevel window for user to login into the system
    '''
    def __init__(self,parent=None, request=None, image_dir=None, subdir=None):
        Toplevel.__init__(self,parent)
        self.parent = parent
        self.request = request
        self.image_dir = image_dir
        self.subdir = subdir
        self.transient(parent)
        self.makewidgets()
        self.grab_set()
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        self.initial_focus = self
        if not (self.initial_focus):
            self.initial_focus = self
        self.initial_focus.focus_set()
        self.wait_window(self)

    def makewidgets(self):
        container = ccframe(self, "LOG IN", "G.TFrame", "G.TButton")
        photo_container = ttk.Frame(container.body, width=100)
        lbl = pImage(photo_container, self.image_dir,'profile.jpeg', self.subdir, size=(300,250))
        lbl.pack(fill=BOTH)
        user_container = Frame(container.body)
        user_container.configure(background='white')
        btns_container = Frame(user_container)
        btns_container.configure(background='white')
        self.username = makeFormRowString(user_container, 'username'.upper(), browse=False, focus=True)
        self.password = makeFormRowString(user_container, 'password'.upper(), browse=False, passwd=True)

        #################
        # action buttton
        #################

        login = ttk.Button(btns_container, text='log in'.upper(), style='S.TButton', command=self.user_login)
        register = ttk.Button(btns_container, text='sign up'.upper(), style='L.TButton', command=self.reg_user)

        ####################
        # bind event
        ####################
        self.bind('<Return>', self.user_login)
        self.bind('<Escape>', lambda event:self.destroy())

        ##################
        # config and pack
        ##################
        login.pack(side=RIGHT, padx=2, pady=5)
        register.pack(side=RIGHT, padx=2, pady=5)

        photo_container.pack(expand=YES, fill=BOTH, side=LEFT)
        user_container.pack(expand=YES, fill=X, padx=5, pady=5)
        btns_container.pack(expand=YES, fill=X, padx=5, pady=5)
        container.pack(fill=BOTH, expand=YES, padx=5, pady=5)
    
    def user_login(self, e=None):
        '''
        Authenticate the User and log him or her into the system
        '''
        username = None
        password = None
        try:
            username = self.username.get()
            password = self.password.get()
        except KeyError:
            showwarning('Log in', 'please input both the username and password')
        if not (username and password):
            return
        user = authenticate(username=username, password=password)
        self.request.user = user
        if user :
            if user.is_active:
                login(self.request, user)
                self.destroy()
            else:
                showerror('Login Error', 'Invalid logins please check your username and password')
        else:
            showerror('Login Error', 'Invalid logins please check your username and password')
    
    def reg_user(self):
        '''
        registers a new user in the database
        '''
        dlg = registerUser(self,self.request, self.image_dir, self.subdir)

if __name__ == "__main__":
    loginDialog().mainloop()



