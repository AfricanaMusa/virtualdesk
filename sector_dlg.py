#!/usr/bin/env python
'''
sector edit form
'''

from tkinter import *
from tkinter import ttk
from tkinter.messagebox import *
from django.contrib.auth.models import User
from virtualdesk.models import Department
from tools.formrows import *
from tools.process_img import pImage
from tools.widgets import cframe, ccframe

class createSector(Toplevel):
    '''
    form for editing sector object 
    '''
    def __init__(self,parent=None, request=None, image_dir=None, subdir=None):
        Toplevel.__init__(self,parent)
        self.parent = parent
        self.request = request
        self.image_dir = image_dir
        self.subdir = subdir
        self.transient(parent)
        self.makewidgets()
        self.grab_set()
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        self.initial_focus = self
        if not (self.initial_focus):
            self.initial_focus = self
        self.initial_focus.focus_set()
        self.wait_window(self)
    
    def makewidgets(self):
        ############
        # containers
        ############

        container = ccframe(self, "LOG IN", "G.TFrame", "G.TButton")
        photo_container = ttk.Frame(container.body, width=100)
        lbl = pImage(photo_container, self.image_dir,'photo-register.jpeg', self.subdir)
        lbl.pack(fill=BOTH)
        user_container = Frame(container.body)
        user_container.configure(background='white')
        btns_container = Frame(container.body)
        btns_container.configure(background='white')

        ####################
        # forms
        ####################
        self.name = makeFormRowString(user_container, 'name'.upper(), browse=False,focus=True)
        self.description = makeFormRowString(user_container, 'description'.upper(), browse=False)

        ####################
        # bind event
        ####################
        self.bind('<Return>', self.create_sector)
        self.bind('<Escape>', lambda event:self.destroy())

        ###################
        # action buttons
        ###################

        update = ttk.Button(btns_container, text='create'.upper(), style="L.TButton", command=self.create_sector)
        cancel = ttk.Button(btns_container, text='clear'.upper(), style="S.TButton")
        close = ttk.Button(btns_container, text='exit'.upper(), command=self.destroy, style="S.TButton")

        #####################
        # config and packing
        #####################
        photo_container.pack(expand=YES, fill=BOTH,side=LEFT)
        user_container.pack(expand=YES, fill=BOTH, padx=5)

        close.pack(side=RIGHT, fill=X, padx=4,pady=4)
        cancel.pack(side=RIGHT, fill=X, padx=4,pady=4)
        update.pack(side=RIGHT, fill=X, padx=4,pady=4)

        btns_container.pack(expand=YES, fill=BOTH, padx=5)
        
        container.pack(fill=BOTH, expand=YES, padx=5, pady=5)

    def create_sector(self, event=None):
        '''
        allow user to create sector
        '''
        user = User.objects.get(pk=self.request.user.id)
        if user:
            d = Department(
                name = self.name.get() if self.name.get() != 'name'.upper() else None,
                description = self.description.get() if self.description.get() != 'description'.upper() else None
            )
            d.save()
            showinfo('Sector', 'Department created sucessfully')

        

if __name__ == "__main__":
    editProfile().mainloop()