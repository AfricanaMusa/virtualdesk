#!/usr/bin/env python
'''
########################################################################
wrap up widget construction in fuctions for easier use, base open some
assuptions (e.g., expansion); use **extras fkw args for width, font/color,
e.t.c., and repark result manually later to override defaults if needed;
#########################################################################
'''
from tkinter import *
from tkinter import ttk

def frame(root,side=TOP, **extras):
    widget = Frame(root)
    widget.pack(side=side, expand=NO, fill=X)
    if extras: widget.config(**extras)
    return widget

def radiobutton(root, text, value, side=TOP, **extras):
    widget = Radiobutton(root, text=text, value=value, bd=3)
    widget.pack(side=side, expand=NO, fill=X)
    if extras: widget.config(**extras)
    return widget

class borderedFrame(Frame):
    '''create a frame with border'''
    def __init__(self,parent, bordercolor=None, borderleft=0, bordertop=0, borderright=0, borderbottom=0, interiorwidget=Frame, **kwargs):
        Frame.__init__(self, parent, background=bordercolor, bd=0, highlightthickness=0)

        self.interior = interiorwidget(self, **kwargs)
        self.interior.pack(padx=(borderleft, borderright), pady=(bordertop, borderbottom),fill=X, expand=YES)
        self.pack(anchor=NW, padx=2, pady=2)


class ccframe(ttk.Frame):
    '''
    frame with colored label
    ''' 
    def __init__(self, parent, text,header_color,lbl_color,frame_color = False ,**kwargs):
        ttk.Frame.__init__(self, parent,**kwargs)
        self.text_var = StringVar()
        self.text_var.set(text)
        header = ttk.Frame(self, height=10, style=header_color)
        header.pack(fill=X)
        lbl = ttk.Label(header,textvariable=self.text_var, style=lbl_color, foreground="white")
        lbl.pack(anchor=W)
        body = Frame(self)
        body.config(background='white')
        body.pack(expand=YES, fill=BOTH,padx=2, pady=2)
        self.body = body
        if frame_color:
            self.configure(style=header_color)
        self.pack(expand=YES, fill=BOTH, padx=2, ipadx=2)


class cframe(ttk.Frame):
    '''
    frame with colored label
    ''' 
    def __init__(self, parent, text, **kwargs):
        ttk.Frame.__init__(self, parent,**kwargs)
        self.text_var = StringVar()
        self.text_var.set(text)
        header = ttk.Frame(self, height=10, style="H.TFrame")
        header.pack(fill=X)
        lbl = ttk.Label(header,textvariable=self.text_var, style="R.TButton", foreground="white")
        lbl.pack(anchor=W)
        body = Frame(self)
        body.config(background='white')
        body.pack(expand=YES, fill=BOTH,padx=2, pady=2)
        self.body = body
        self.configure(style="H.TFrame")
        self.pack(expand=YES, fill=BOTH, padx=2, ipadx=2)
        

def label(root,side,text,**extras):
    widget = Label(root,text=text)
    widget.pack(side=side,fill=BOTH)
    if extras: widget.config(**extras)
    return widget

def button(root,text,command,side=TOP,**extras):
    widget = Button(root,text=text,command=command)
    widget.pack(side=side,expand=YES,fill=BOTH)
    if extras: widget.config(**extras)
    return widget
 
def entry(root,side,linkvar,**extras):
    widget = Entry(root,relief=SUNKEN,textvariable=linkvar)
    widget.pack(side=side,expand=YES,fill=BOTH)
    if extras: widget.config(**extras)
    return widget

if __name__ == '__main__':
    app = Tk()
    frm = frame(app)
    s = ttk.Style(app)
    s.configure("C.TFrame", background="blue", bd=4)
    s.configure("R.TButton", background='blue', bd=2, relief=FLAT)
    cframe(frm,"Header One")
    #button(frm, 'Press', lambda : print('pushed'),BOTTOM)
    mainloop()
    