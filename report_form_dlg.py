#!/usr/bin/env python
'''
profile edit form
'''

from tkinter import *
from tkinter import ttk
from tkinter.messagebox import *
from django.contrib.auth.models import User
from tools.formrows import *
from tools.process_img import pImage
from tools.widgets import cframe, ccframe
from virtualdesk.models import Reports

class createReport(Toplevel):
    '''
    form for editing user object and user profile 
    '''
    def __init__(self,parent=None, request=None, image_dir=None, subdir=None):
        Toplevel.__init__(self,parent)
        self.parent = parent
        self.request = request
        self.image_dir = image_dir
        self.subdir = subdir
        self.transient(parent)
        self.makewidgets()
        self.grab_set()
        self.protocol('WM_DELETE_WINDOW', self.destroy)
        self.initial_focus = self
        if not (self.initial_focus):
            self.initial_focus = self
        self.initial_focus.focus_set()
        self.wait_window(self)
    
    def makewidgets(self):
        ############
        # containers
        ############

        container = ccframe(self, "LOG IN", "G.TFrame", "G.TButton")
        photo_container = ttk.Frame(container.body, width=100)
        lbl = pImage(photo_container, self.image_dir,'photo-report.jpg', self.subdir)
        lbl.pack(fill=BOTH)
        user_container = Frame(container.body)
        user_container.configure(background='white')
        profile_container = Frame(container.body)
        profile_container.configure(background='white')
        btns_container = Frame(container.body)
        btns_container.configure(background='white')

        ####################
        # forms
        ####################
        self.report_no = makeFormRowInt(user_container, 'file number'.upper(), browse=False, focus=True)
        self.file_title = makeFormRowString(user_container, 'report title'.upper(), browse=False)
        self.department = makeFormRowString(user_container, 'department'.upper(), browse=False)
        self.created_at = makeFormRowDate(profile_container, 'created at'.upper(), browse=False)
        self.sector = makeFormRowString(profile_container, 'sector'.upper(), browse=False)
        self.report = makeFormRowString(profile_container, 'report'.upper())

        ####################
        # bind event
        ####################
        self.bind('<Return>', self.create_report)
        self.bind('<Escape>', lambda event:self.destroy())

        ###################
        # action buttons
        ###################

        update = ttk.Button(btns_container, text='upload'.upper(), style="L.TButton", command=self.create_report)
        cancel = ttk.Button(btns_container, text='clear'.upper(), style="S.TButton")
        close = ttk.Button(btns_container, text='exit'.upper(), command=self.destroy, style="S.TButton")

        #####################
        # config and packing
        #####################
        photo_container.pack(expand=YES, fill=BOTH,side=LEFT)
        user_container.pack(expand=YES, fill=BOTH, padx=5)
        profile_container.pack(expand=YES, fill=BOTH, padx=5)

        close.pack(side=RIGHT, fill=X, padx=4,pady=4)
        cancel.pack(side=RIGHT, fill=X, padx=4,pady=4)
        update.pack(side=RIGHT, fill=X, padx=4,pady=4)

        btns_container.pack(expand=YES, fill=BOTH, padx=5)
        
        container.pack(fill=BOTH, expand=YES, padx=5, pady=5)

    def create_report(self, event=None):
        '''
        update user and profile table
        '''
        user = User.objects.get(pk=self.request.user.id)
        if user:
            r = Reports(
                report_no = self.report_no.get() if self.report_no.get() != 'file number'.upper() else None,
                file_title =  self.file_title.get() if self.file_title.get() != 'report title'.upper() else None,
                department = self.department.get() if self.department.get() != 'department'.upper() else None,
                sector = self.sector.get() if self.sector.get() != 'sector'.upper() else None,
                created_at = self.created_at.get() if self.created_at.get() != 'created at'.upper() else None,
                file = self.report.get() if self.report.get() != 'report'.upper() else None,
                created_by = user,
            )
            r.save()
            showinfo("Report", "Report sucessfully uploaded")


            


if __name__ == "__main__":
    editProfile().mainloop()

